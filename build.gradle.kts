import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// Nexus credentials
val nexusUrl: String by project.extra
val nexusUser: String by project.extra
val nexusPassword: String by project.extra
val releasesRepo: String by project.extra
val snapshotsRepo: String by project.extra
val mavenProxyUrl: String by project.extra

val jacksonModuleKotlin: String by project.extra
val mockKVersion: String by project.extra
val bouncyCastleVersion: String by project.extra
val springmockkVersion: String by project.extra
val jsonVersion: String by project.extra
val commonsIoVersion: String by project.extra
val springBootVersion: String by project.extra
val snakeyamlVersion: String by project.extra

repositories {
    val mavenProxyUrl: String by extra

    mavenLocal()
    maven { url = uri(mavenProxyUrl); isAllowInsecureProtocol = true }

    mavenCentral()
    gradlePluginPortal()
}

plugins {
    val kotlinVersion = "1.9.22"
    val dependencyManagementPluginVersion = "1.0.9.RELEASE"

    id("io.spring.dependency-management") version dependencyManagementPluginVersion
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.spring") version kotlinVersion
    id("maven-publish")
    id("jacoco")
    id("org.sonarqube") version "4.2.1.3168"
}

dependencyManagement {
    val springBootVersion: String by extra
    val kotlinVersion: String by extra

    imports {
        mavenBom("org.springframework.boot:spring-boot-dependencies:$springBootVersion") {
            // alignement des versions de kotlins
            bomProperty("kotlin.version", kotlinVersion)
        }
    }
}

dependencies {
    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    // Jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonModuleKotlin")

    // Spring
    implementation("org.springframework.boot:spring-boot-starter:$springBootVersion")
    implementation("org.springframework.boot:spring-boot-starter-webflux:$springBootVersion")
    implementation("org.yaml:snakeyaml:$snakeyamlVersion")

    // Tezos
    implementation("org.bouncycastle:bcprov-jdk15on:$bouncyCastleVersion")
    implementation("org.json:json:$jsonVersion")
    implementation("commons-io:commons-io:$commonsIoVersion")

    // Testing
    testImplementation("org.springframework.boot:spring-boot-starter-test:$springBootVersion") {
        exclude(module = "mockito-core")
    }
    testImplementation("com.ninja-squad:springmockk:$springmockkVersion")
    testImplementation("io.mockk:mockk:$mockKVersion")
}

configurations {
    all {
        exclude(group = "org.springframework.boot", module = "snakeyaml")
    }
}

group = "eu.coexya.tezos"
version = "3.4.6"
description = "Tezos connectors implementation"

tasks {
    jar {
        enabled = true
    }

    withType<Test> {
        useJUnitPlatform()
        jvmArgs = listOf("-Djdk.tls.client.protocols=TLSv1.2")
    }

    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "17"
        }
    }

    val sourcesJar by registering(Jar::class) {
        archiveClassifier.set("sources")
        from(sourceSets.main.get().allSource)
    }
}

jacoco {
    val jacocoToolVersion: String by extra
    toolVersion = jacocoToolVersion
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = "tezos-node-connector"
            from(components["kotlin"])
            artifact(tasks["sourcesJar"])
        }
    }

    repositories {
        maven {
            credentials {
                username = nexusUser
                password = nexusPassword
            }

            val releasesRepoUrl = "${nexusUrl}/repository/${releasesRepo}"
            val snapshotsRepoUrl = "${nexusUrl}/repository/${snapshotsRepo}"
            url = if (version.toString().endsWith("SNAPSHOT")) {
                uri(snapshotsRepoUrl)
            } else {
                uri(releasesRepoUrl)
            }
            // Allow HTTP repository
            isAllowInsecureProtocol = true
        }
    }
}
