package eu.coexya.tezos.connector.contract

import eu.coexya.tezos.connector.enums.Type
import eu.coexya.tezos.connector.model.OperationParameters
import eu.coexya.tezos.connector.model.TezosIdentity
import eu.coexya.tezos.connector.service.TezosAbstractService
import eu.coexya.tezos.connector.service.TezosCryptoService
import eu.coexya.tezos.connector.service.TezosReaderService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class TransferService(
    override val tezosReaderService: TezosReaderService,
    override val tezosCryptoService: TezosCryptoService,
    @Value("\${tezos.contract.address: #{null}}") override val tezosContractAddress: String?,
    @Value("\${tezos.node.backupUrls:#{null}}") override val backupUrls: List<String>?,
    @Value("\${tezos.node.url}") override val mainUrl: String,
) : TezosAbstractService {

    /**
     * Transfer Tz from an account to another
     * @param from the account debited
     * @param to    the public address of the credited account
     * @param amount the amount of Tz transferred (in micro tez, 1 tez = 1 000 000 micro tez)
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    suspend fun transfer(from: TezosIdentity, to: String, amount: Long): String {

        val parameters = OperationParameters(
            type = Type.TRANSFER,
            accounts = listOf(to),
            amounts = listOf(amount.toString()),
        )

        val transactionHash = invoke(parameters, from)

        LOGGER.info(
            "transfer result (transactionHash = {})",
            transactionHash
        )

        return transactionHash
    }


    companion object {
        private val LOGGER = LoggerFactory.getLogger(TransferService::class.java)
    }
}
