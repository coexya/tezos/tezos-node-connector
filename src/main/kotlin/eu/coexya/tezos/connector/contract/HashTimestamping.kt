package eu.coexya.tezos.connector.contract

import eu.coexya.tezos.connector.enums.Type
import eu.coexya.tezos.connector.model.OperationParameters
import eu.coexya.tezos.connector.model.TezosIdentity
import eu.coexya.tezos.connector.service.TezosAbstractService
import eu.coexya.tezos.connector.service.TezosCryptoService
import eu.coexya.tezos.connector.service.TezosReaderService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class HashTimestamping(
    @Value("\${tezos.contract.entrypoint: default}")
    private val contractEntrypoint: String,
    override val tezosReaderService: TezosReaderService,
    override val tezosCryptoService: TezosCryptoService,
    @Value("\${tezos.contract.address: #{null}}") override val tezosContractAddress: String?,
    @Value("\${tezos.node.backupUrls:#{null}}") override val backupUrls: List<String>?,
    @Value("\${tezos.node.url}") override val mainUrl: String,
) : TezosAbstractService {

    suspend fun timestampHash(rootHashes: List<String>, tezosIdentity: TezosIdentity): String {
        LOGGER.debug("timestampHash call (rootHash = {})", rootHashes)

        checkNotNull(tezosContractAddress)

        val parameters = OperationParameters(
            type = Type.TRANSACTION,
            hashesBySC = mapOf(tezosContractAddress!! to rootHashes),
            entrypoint = contractEntrypoint,
        )

        val transactionHash = invoke(parameters, tezosIdentity)

        LOGGER.info("timestampHash result (rootHash = {}, transactionHash= {})", rootHashes, transactionHash)
        return transactionHash
    }

    suspend fun timestampHashMultiSmartContract(
        rootHashesMultiSmartContract: Map<String, List<String>>,
        tezosIdentity: TezosIdentity,
    ): String {

        LOGGER.debug("timestampHashMultiSmartContract call (rootHash = {})", rootHashesMultiSmartContract)

        val parameters = OperationParameters(
            type = Type.TRANSACTION,
            hashesBySC = rootHashesMultiSmartContract,
            entrypoint = contractEntrypoint,
        )

        val transactionHash = invoke(parameters, tezosIdentity)

        val rootHashesList = rootHashesMultiSmartContract.values.flatten()
        LOGGER.info("timestampHash result (rootHash = {}, transactionHash= {})", rootHashesList, transactionHash)

        return transactionHash
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(HashTimestamping::class.java)
    }
}
