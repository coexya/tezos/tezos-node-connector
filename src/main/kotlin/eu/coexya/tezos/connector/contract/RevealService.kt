package eu.coexya.tezos.connector.contract

import eu.coexya.tezos.connector.enums.Type
import eu.coexya.tezos.connector.model.OperationParameters
import eu.coexya.tezos.connector.model.TezosIdentity
import eu.coexya.tezos.connector.service.TezosAbstractService
import eu.coexya.tezos.connector.service.TezosCryptoService
import eu.coexya.tezos.connector.service.TezosReaderService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class RevealService(
    override val tezosReaderService: TezosReaderService,
    override val tezosCryptoService: TezosCryptoService,
    @Value("\${tezos.contract.address: #{null}}") override val tezosContractAddress: String?,
    @Value("\${tezos.node.backupUrls:#{null}}") override val backupUrls: List<String>?,
    @Value("\${tezos.node.url}") override val mainUrl: String,
) : TezosAbstractService {

    /**
     * Reveals an account
     * @param identity the account debited
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    suspend fun reveal(identity: TezosIdentity): String {
        LOGGER.debug("reveal call (identity = {}", identity)

        val parameters = OperationParameters(
            type = Type.REVEAL,
            publicKeys = listOf(identity.publicKey),
        )

        val transactionHash = invoke(parameters, identity)

        LOGGER.info(
            "reveal result (transactionHash = {})",
            transactionHash
        )

        return transactionHash
    }


    companion object {
        private val LOGGER = LoggerFactory.getLogger(RevealService::class.java)
    }
}
