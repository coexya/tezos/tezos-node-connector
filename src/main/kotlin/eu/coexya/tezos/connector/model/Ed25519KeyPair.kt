package eu.coexya.tezos.connector.model

data class Ed25519KeyPair(
    val publicKey: ByteArray,
    val privateKey: ByteArray,
)
