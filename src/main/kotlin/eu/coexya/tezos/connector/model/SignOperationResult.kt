package eu.coexya.tezos.connector.model

data class SignOperationResult(
    val encodedSignature: String,
    val rawSignatureAsHex: String,
)
