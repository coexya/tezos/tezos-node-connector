package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonProperty

data class RunOperation(
    @JsonProperty("operation")
    val operation: Operation,
    @JsonProperty("chain_id")
    val chain_id: String,
)
