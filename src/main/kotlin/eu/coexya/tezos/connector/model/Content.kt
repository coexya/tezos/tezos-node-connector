package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(Include.NON_NULL)
data class Content(
    @JsonProperty("kind")
    val kind: String,
    @JsonProperty("source")
    val source: String,
    @JsonProperty("fee")
    var fee: String = "0",
    @JsonProperty("counter")
    val counter: String,
    @JsonProperty("gas_limit")
    val gas_limit: String = "3000",
    @JsonProperty("storage_limit")
    val storage_limit: String = "100",
    @JsonProperty("public_key")
    val public_key: String? = null,
    @JsonProperty("amount")
    val amount: String? = null,
    @JsonProperty("destination")
    val destination: String? = null,
    @JsonProperty("parameters")
    val parameters: Parameters? = null,
    @JsonProperty("metadata")
    val metadata: Metadata? = null,
)
