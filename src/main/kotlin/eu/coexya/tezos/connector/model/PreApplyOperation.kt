package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonProperty

data class PreApplyOperation(
    @JsonProperty("protocol")
    val protocol: String,
    @JsonProperty("branch")
    val branch: String,
    @JsonProperty("contents")
    val contents: MutableList<Content>,
    @JsonProperty("signature")
    val signature: String,
)
