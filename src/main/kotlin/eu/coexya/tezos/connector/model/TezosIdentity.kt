package eu.coexya.tezos.connector.model

data class TezosIdentity(
    val publicKey: String,
    val privateKey: String,
    val publicAddress: String,
)
