package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
data class RunAndPreApplyOperationResult(
    @JsonProperty("contents")
    val contents: List<Content>,
    @JsonProperty("signature")
    val signature: String,
    @JsonProperty("blockHash")
    var blockHash: String? = null,
)