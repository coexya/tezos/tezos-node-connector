package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonProperty

data class ForgeOp(
    @JsonProperty("contents")
    val contents: MutableList<Content>,
    @JsonProperty("branch")
    val branch: String,
)
