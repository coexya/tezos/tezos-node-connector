package eu.coexya.tezos.connector.model

data class ForgeOperationResult(
    val result: String,
    val feesList: List<Long>,
)
