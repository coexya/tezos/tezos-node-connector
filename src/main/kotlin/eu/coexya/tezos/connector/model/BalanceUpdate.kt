package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonProperty

data class BalanceUpdate(
    @JsonProperty("kind")
    val kind: String,
    @JsonProperty("contract")
    val contract: String,
    @JsonProperty("change")
    val change: String,
    @JsonProperty("origin")
    val origin: String,
)
