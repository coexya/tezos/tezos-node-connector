package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Parameters(
    @JsonProperty("entrypoint")
    val entrypoint: String = "default",
    @JsonProperty("value")
    val value: Value,
)
