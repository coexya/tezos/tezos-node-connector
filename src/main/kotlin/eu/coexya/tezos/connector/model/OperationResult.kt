package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
data class OperationResult(
    @JsonProperty("status")
    val status: String,
    @JsonProperty("consumed_gas")
    val consumed_gas: String? = null,
    @JsonProperty("consumed_milligas")
    val consumed_milligas: String? = null,
    @JsonProperty("storage_size")
    val storage_size: String? = null,
    @JsonProperty("paid_storage_size_diff")
    val paid_storage_size_diff: String? = null,
)
