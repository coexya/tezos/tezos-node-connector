package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Metadata(
    @JsonProperty("operation_result")
    val operation_result: OperationResult,
)
