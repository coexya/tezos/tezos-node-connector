package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Value(
    @JsonProperty("string")
    val string: String
)
