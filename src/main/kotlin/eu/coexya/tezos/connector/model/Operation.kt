package eu.coexya.tezos.connector.model

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Operation(
    @JsonProperty("contents")
    var contents: List<Content>,
    @JsonProperty("branch")
    val branch: String,
    @JsonProperty("signature")
    val signature: String? = null,
)
