package eu.coexya.tezos.connector.model

import eu.coexya.tezos.connector.enums.Type

data class OperationParameters(
    val type: Type,
    val hashesBySC: Map<String, List<String>> = emptyMap(),
    val entrypoint: String = "",
    val publicKeys: List<String> = emptyList(),
    val accounts: List<String> = emptyList(),
    val amounts: List<String> = emptyList(),
)
