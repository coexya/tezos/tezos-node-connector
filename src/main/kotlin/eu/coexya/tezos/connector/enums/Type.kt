package eu.coexya.tezos.connector.enums

enum class Type(val type: String) {
    TRANSACTION("transaction"),
    REVEAL("reveal"),
    TRANSFER("transfer"),
}