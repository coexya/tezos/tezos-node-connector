package eu.coexya.tezos.connector.enums

enum class EncoderType {
    HEX,
    BASE58,
}