package eu.coexya.tezos.connector.node.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.OffsetDateTime

data class TzBlock(
    @JsonProperty("hash")
    val hash: String,
    @JsonProperty("header")
    val header: Header,
    @JsonProperty("metadata")
    val metadata:  Map<String, Any> = emptyMap(),
    @JsonProperty("operations")
    val operations: List<List<TzOperation>>,
) {
    data class Header(
        @JsonProperty("level")
        val level: Long,
        @JsonProperty("timestamp")
        val timestamp: OffsetDateTime
    )
}
fun TzBlock.getBakerAddress() = metadata["baker"] as String
fun TzBlock.getCycle() = (metadata["level_info"] as Map<*, *>)["cycle"] as Int
