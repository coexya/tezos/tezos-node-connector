package eu.coexya.tezos.connector.node.model

data class BakingRight(
    val level: Long,
    val delegate: String,
    val priority: Int,
)
