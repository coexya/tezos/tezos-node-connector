package eu.coexya.tezos.connector.node.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.OffsetDateTime

data class TzBlockHeaderResponse(
    @JsonProperty("level")
    val level: Long,
    @JsonProperty("timestamp")
    val timestamp: OffsetDateTime,
)
