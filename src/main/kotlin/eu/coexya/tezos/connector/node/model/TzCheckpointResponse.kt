package eu.coexya.tezos.connector.node.model

import com.fasterxml.jackson.annotation.JsonProperty

data class TzCheckpointResponse(
    @JsonProperty("history_mode")
    val historyMode: HistoryMode
)

