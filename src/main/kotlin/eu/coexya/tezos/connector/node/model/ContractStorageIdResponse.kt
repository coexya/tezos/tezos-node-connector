package eu.coexya.tezos.connector.node.model

import com.fasterxml.jackson.annotation.JsonProperty

data class ContractStorageIdResponse(
    @JsonProperty("int")
    val int: String
)

