package eu.coexya.tezos.connector.node.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import eu.coexya.tezos.connector.node.BigMapIdDeserializer

@JsonDeserialize(using = BigMapIdDeserializer::class)
data class BigMapId(
    val int: String?
)