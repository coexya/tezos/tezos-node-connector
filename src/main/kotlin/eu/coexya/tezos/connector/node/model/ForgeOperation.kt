package eu.coexya.tezos.connector.node.model

data class ForgeOperation(
    val contents: List<Map<String,Any>>,
    val branch: String,
)
