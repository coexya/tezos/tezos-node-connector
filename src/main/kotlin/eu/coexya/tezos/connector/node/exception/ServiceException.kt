package eu.coexya.tezos.connector.node.exception

/**
 * Server errors.
 * Translated to HTTP 5xx errors.
 */
open class ServiceException(message: String) : RuntimeException(message) {
}

class AddressNotFoundException(tezosError: String) : ServiceException(tezosError)

class PreApplyFailedException(tezosError: String) : ServiceException(tezosError)

class OperationFailedException(tezosError: String) : ServiceException(tezosError)
