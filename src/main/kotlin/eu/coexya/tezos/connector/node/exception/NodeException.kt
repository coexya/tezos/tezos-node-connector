package eu.coexya.tezos.connector.node.exception

sealed class NodeException(message: String) : RuntimeException(message) {

    class NodeAccessException : NodeException("All the nodes were unreachable or produced an error")

    class NodeDeSyncException(host: String) : NodeException("The node at $host is de-synchronized")

    class NotFoundException : NodeException("The resource doesn't exist.")

}
