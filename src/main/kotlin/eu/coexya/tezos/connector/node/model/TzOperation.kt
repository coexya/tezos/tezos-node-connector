package eu.coexya.tezos.connector.node.model

import com.fasterxml.jackson.annotation.JsonProperty

data class TzOperation(
    @JsonProperty("hash")
    val hash: String,
    @JsonProperty("contents")
    val contents: List<Map<String, Any>> = emptyList(),
    @JsonProperty("branch")
    val branch: String,
    @JsonProperty("signature")
    val signature: String?,
)

class OperationKind {
    companion object {
        const val TRANSACTION = "transaction"
        const val ENDORSEMENT = "endorsement"
        const val REVEAL = "reveal"
    }
}

fun TzOperation.totalFee() = contents.sumOf {
    it.getLong("fee")
}

fun TzOperation.totalConsumedGas() =
    contents.sumOf {
        it.getValueInOperationResult("consumed_gas")?.toLong() ?: 0
    }

fun TzOperation.totalStorageSize() =
    contents.sumOf {
        it.getValueInOperationResult("storage_size")?.toLong() ?: 0
    }

fun TzOperation.isApplied(): Boolean =
    contents.all {
        it.getValueInOperationResult("status") == "applied"
    }

fun Map<String, Any>.getLong(key: String): Long = if (containsKey(key)) (this[key] as String).toLong() else 0

fun Map<String, Any>.getValueInOperationResult(key: String): String? =
    if (
        this.containsKey("metadata")
        && (this["metadata"] as Map<*, *>).containsKey("operation_result")
        && ((this["metadata"] as Map<*, *>)["operation_result"] as Map<*, *>).containsKey(key)
    ) {
        (((this["metadata"] as Map<*, *>)["operation_result"] as Map<*, *>)[key] as String)
    } else {
        null
    }
