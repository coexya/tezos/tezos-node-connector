package eu.coexya.tezos.connector.node.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Protocols(
    @JsonProperty("protocol")
    val protocol: String,
    @JsonProperty("next_protocol")
    val nextProtocol: String,
)
