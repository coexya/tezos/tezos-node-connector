package eu.coexya.tezos.connector.node.model

data class PreApplyResponse(
    val contents: List<Map<String, Any>>,
    val signature: String,
)

fun PreApplyResponse.totalFee() = contents.sumOf {
    (it["fee"] as String).toLong()
}

fun PreApplyResponse.totalConsumedGas() =
    contents.sumOf {
        (((it["metadata"] as Map<*, *>)["operation_result"] as Map<*, *>)["consumed_gas"] as String?)?.toLong() ?: 0
    }

fun PreApplyResponse.totalStorageSize() =
    contents.sumOf {
        (((it["metadata"] as Map<*, *>)["operation_result"] as Map<*, *>)["storage_size"] as String?)?.toLong() ?: 0
    }
