package eu.coexya.tezos.connector.node.model

import com.fasterxml.jackson.annotation.JsonProperty

enum class HistoryMode(val level: Int) {
    @JsonProperty("rolling")
    ROLLING(0),
    @JsonProperty("full")
    FULL(1),
    @JsonProperty("archive")
    ARCHIVE(2),
}