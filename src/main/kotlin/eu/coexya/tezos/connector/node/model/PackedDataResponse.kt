package eu.coexya.tezos.connector.node.model

import com.fasterxml.jackson.annotation.JsonProperty

data class PackedDataResponse(
    @JsonProperty("packed")
    val packedData: String,
    @JsonProperty("gas")
    val gas: String
)
