package eu.coexya.tezos.connector.node

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import eu.coexya.tezos.connector.node.model.BigMapId

class BigMapIdDeserializer : StdDeserializer<BigMapId>(BigMapId::class.java) {

    override fun deserialize(jsonParser: JsonParser, deserializationContext: DeserializationContext): BigMapId {
        val node: JsonNode = jsonParser.codec.readTree(jsonParser)
        val codeStorageNode: JsonNode = node.get("code").get(1).get("args").get(0)
        val storageNode: JsonNode = node.get("storage")
        return if (codeStorageNode.get("prim").asText() == "big_map") {
            BigMapId(storageNode.get("int").asText())
        } else {
            BigMapId(searchBigMapId(codeStorageNode.get("args"), storageNode.get("args")))
        }
    }

    private fun searchBigMapId(codeStorageNode: JsonNode, storageNode: JsonNode): String? {
        var bigMapId: String? = null
        for (i in 0..codeStorageNode.size()) {
            if (codeStorageNode.get(i).get("prim").asText() == "big_map") {
                bigMapId = storageNode.get(i).get("int").asText()
            } else if (codeStorageNode.get(i).get("prim").asText() == "pair") {
                bigMapId = searchBigMapId(codeStorageNode.get(i).get("args"), storageNode.get(i).get("args"))
            }
            if (bigMapId != null) return bigMapId
        }
        return null
    }
}