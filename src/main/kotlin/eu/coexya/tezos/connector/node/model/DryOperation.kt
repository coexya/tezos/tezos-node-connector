package eu.coexya.tezos.connector.node.model

data class DryOperation(
    val protocol: String,
    val contents: List<Map<String,Any>>,
    val branch: String,
    val signature: String,
)
