package eu.coexya.tezos.connector.node.model

data class PackedDataRequest(
    val data: Element,
    val type: Type = Type()
) {
    data class Element(val string: String)
    data class Type(val prim: String = "string")
}
