package eu.coexya.tezos.connector.node

import eu.coexya.tezos.connector.model.ForgeOp
import eu.coexya.tezos.connector.model.PreApplyOperation
import eu.coexya.tezos.connector.model.RunAndPreApplyOperationResult
import eu.coexya.tezos.connector.model.RunOperation
import eu.coexya.tezos.connector.node.exception.NodeException
import eu.coexya.tezos.connector.node.exception.OperationFailedException
import eu.coexya.tezos.connector.node.exception.PreApplyFailedException
import eu.coexya.tezos.connector.node.model.*
import eu.coexya.tezos.connector.utils.HEAD_BLOCK
import eu.coexya.tezos.connector.utils.changeBaseUrl
import kotlinx.coroutines.reactive.awaitSingle
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatusCode
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitExchange
import reactor.core.publisher.Mono
import java.time.Duration
import java.time.OffsetDateTime

@Component
class NodeConnector(
    private val nodeWebClient: WebClient,
    @Value("\${tezos.node.backupUrls:#{null}}") val backupUrls: List<String>?,
    @Value("\${tezos.node.url}") val mainUrl: String,
    @Value("\${tezos.node.syncThreshold:5m}") val syncThreshold: Duration,
    @Value("\${tezos.node.syncCheckCacheDuration:5m}") val syncCheckCacheDuration: Duration,
) {

    data class CachedValue(val blockTimestamp: OffsetDateTime, val lastCheckDate: OffsetDateTime)

    val syncCheckCache = HashMap<String, CachedValue>()

    /**
     * Gets balance from given public address, null if the account doesn't exist on the blockchain.
     * @param tezosPublicAddress Public address of the account to check
     * @param host optional param to force the request to specific host tezos node
     */
    suspend fun getBalanceOrNull(tezosPublicAddress: String, host: String? = null): Long? {
        return genericGet("/chains/main/blocks/head/context/contracts/$tezosPublicAddress/balance", host)
    }

    /**
     * Gets balance from given public address, 0L if the account doesn't exist on the blockchain.
     * @param tezosPublicAddress Public address of the account to check
     */
    suspend fun getBalance(tezosPublicAddress: String): Long {
        return getBalanceOrNull(tezosPublicAddress) ?: 0L
    }

    /**
     * Gets counter from given public address, -1L if the account doesn't exist on the blockchain.
     * @param tezosPublicAddress Public address of the account to check
     * @param host optional param to force the request to specific host tezos node
     */
    suspend fun getCounter(tezosPublicAddress: String, host: String? = null): Long {
        return genericGet("/chains/main/blocks/head/context/contracts/$tezosPublicAddress/counter", host) ?: -1L
    }

    /**
     * Gets level of given block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     * @param predecessorOffset Offset to get a predecessor of the given blockHash
     */
    suspend fun getBlockLevel(blockHash: String, predecessorOffset: Long = 0L): Long? {
        val blockId = getBlockId(blockHash, predecessorOffset)
        return genericGet<TzBlockHeaderResponse>("/chains/main/blocks/$blockId/header")?.level
    }

    /**
     * Gets hashes of the operations on a given block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     * @param predecessorOffset Offset to get a predecessor of the given blockHash
     */
    suspend fun getBlockOperationHashes(blockHash: String, predecessorOffset: Long = 0L): List<String>? {
        val blockId = getBlockId(blockHash, predecessorOffset)
        return genericGet("/chains/main/blocks/$blockId/operation_hashes/3")
    }

    /**
     * Gets hash of the given block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     * @param predecessorOffset Offset to get a predecessor of the given blockHash
     */
    suspend fun getBlockHash(blockHash: String, predecessorOffset: Long = 0L): String? {
        val blockId = getBlockId(blockHash, predecessorOffset)
        return genericGet<String>("/chains/main/blocks/$blockId/hash")?.trim()?.trim('"')
    }

    /**
     * Gets the block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     * @param predecessorOffset Offset to get a predecessor of the given blockHash
     * @param host optional param to force the request to specific host tezos node
     */
    suspend fun getBlock(blockHash: String, predecessorOffset: Long = 0L, host: String? = null): TzBlock? {
        val blockId = getBlockId(blockHash, predecessorOffset)
        return genericGet("/chains/main/blocks/$blockId", host)
    }

    /**
     * Gets the block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     */
    suspend fun getBlockString(blockHash: String): String? {
        return genericGet("/chains/main/blocks/$blockHash")
    }

    /**
     * Gets operations on a given block, empty list if the block is not on the blockchain
     * @param contractAddress Smart contract concerned by the operation
     * @param blockHash Hash of the block
     */
    @Suppress("UNCHECKED_CAST")
    suspend fun getBlockOperations(contractAddress: String, blockHash: String): List<TzOperationResponse> {
        val list = genericGet<List<TzOperationResponseAPI>>("/chains/main/blocks/$blockHash/operations/3")
            ?.map {
                it.toReadableObject()
            }?.filter {
                it != null && it.destination == contractAddress // Removes all null values.
            }

        return if (list == null) {
            emptyList()
        } else {
            list as List<TzOperationResponse>
        }
    }

    suspend fun getValueFromStorage(storageId: String, scriptExpr: String): StorageResponse? {
        if (storageId.isBlank() || scriptExpr.isBlank()) return null
        return genericGet("/chains/main/blocks/head/context/big_maps/$storageId/$scriptExpr")
    }

    suspend fun getStorageId(contractAddress: String): String? {
        return genericGet<ContractStorageIdResponse>("/chains/main/blocks/head/context/contracts/$contractAddress/storage")?.int
    }

    suspend fun getChainId(host: String? = null): String {
        return genericGet<String>("/chains/main/chain_id", host)!!.trim().trim('"')
    }

    suspend fun packData(hash: String): PackedDataResponse {
        // TODO use generic post
        var baseUrl = mainUrl
        var succeeded = false
        lateinit var answer: PackedDataResponse
        while (!succeeded) {
            try {
                LOGGER.debug("Trying to reach the node $baseUrl")
                answer = nodeWebClient.post()
                    .uri("$baseUrl/chains/main/blocks/head/helpers/scripts/pack_data")
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .body(
                        BodyInserters.fromValue(
                            PackedDataRequest(
                                data = PackedDataRequest.Element(hash)
                            )
                        )
                    )
                    .awaitExchange {
                        when {
                            it.statusCode().is2xxSuccessful -> it.awaitBody()
                            else -> throw NodeException.NodeAccessException()
                        }
                    }
                LOGGER.debug("The node $baseUrl has been reached")
                succeeded = true
            } catch (e: Exception) {
                LOGGER.error("$baseUrl is unreachable or produced an error.")
                baseUrl = changeBaseUrl(baseUrl, backupUrls)
            }
        }
        return answer
    }

    suspend fun isBakingSlot(delegateAddress: String, level: Long): Boolean {
        return genericGet<Array<BakingRight>>(
            "/chains/main/blocks/head/helpers/baking_rights" +
                "?delegate=$delegateAddress" +
                "&max_priority=0&level=$level"
        )
            ?.any { it.delegate == delegateAddress } ?: false
    }

    suspend fun getBakingSlots(delegateAddress: String, priority: Int, cycle: Int): List<BakingRight>? {
        return genericGet(
            "/chains/main/blocks/head/helpers/baking_rights" +
                "?delegate=$delegateAddress" +
                "&max_priority=$priority" +
                "&cycle=$cycle"
        )
    }

    suspend fun dryRun(operation: TzOperation): PreApplyResponse {
        val dryOperation = DryOperation(
            protocol = getProtocols()?.protocol ?: throw NodeException.NodeAccessException(),
            contents = operation.contents,
            branch = operation.branch,
            signature = operation.signature
                ?: throw IllegalArgumentException("Operation' signature should be defined."),
        )
        // TODO use generic post
        var baseUrl = mainUrl
        var succeeded = false
        lateinit var answer: PreApplyResponse
        while (!succeeded) {
            try {
                LOGGER.debug("Trying to reach the node $baseUrl")
                answer = nodeWebClient.post()
                    .uri("$baseUrl/chains/main/blocks/head/helpers/preapply/operations")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .body(Mono.just(listOf(dryOperation)), List::class.java)
                    .retrieve()
                    .onStatus(
                        HttpStatusCode::is5xxServerError
                    ) { error -> error.bodyToMono(String::class.java).map { PreApplyFailedException(it) } }
                    .onStatus(
                        HttpStatusCode::is4xxClientError
                    ) { error -> error.bodyToMono(String::class.java).map { PreApplyFailedException(it) } }
                    .bodyToMono(Array<PreApplyResponse>::class.java).awaitSingle()[0]
                LOGGER.debug("The node $baseUrl has been reached")
                succeeded = true
            } catch (e: Exception) {
                LOGGER.debug("$baseUrl is unreachable or produced an error.")
                baseUrl = changeBaseUrl(baseUrl, backupUrls)
            }
        }
        return answer
    }

    suspend fun runOperation(runOperation: RunOperation, host: String? = null): RunAndPreApplyOperationResult {
        return genericPost(
            "/chains/main/blocks/head/helpers/scripts/run_operation",
            runOperation,
            host,
        )
    }

    suspend fun forgeOperation(forgeOperation: ForgeOp, host: String? = null): String {
        return genericPost<ForgeOp, String>(
            "/chains/main/blocks/head/helpers/forge/operations",
            forgeOperation,
            host,
        ).trim().trim('"')
    }

    suspend fun preApplyOperation(preApplyOperation: PreApplyOperation, host: String? = null): String {
        return genericPost(
            "/chains/main/blocks/head/helpers/preapply/operations",
            listOf(preApplyOperation),
            host,
        )
    }

    suspend fun injectOperation(injectOperation: String, host: String? = null): String {
        return genericPost<String, String>(
            "/injection/operation?chain=main",
            injectOperation,
            host,
        ).trim().trim('"')
    }

    suspend fun getProtocols(host: String? = null): Protocols? {
        return genericGet("/chains/main/blocks/head/protocols", host)
    }

    private suspend inline fun <reified BodyType : Any, reified ResponseType> genericPost(
        endpoint: String,
        body: BodyType,
        host: String? = null
    ): ResponseType {
        return if (host != null) {
            post(host, endpoint, body)
        } else {
            var baseUrl = mainUrl
            var answer: ResponseType? = null

            var succeeded = false

            while (!succeeded) {
                try {
                    answer = post(baseUrl, endpoint, body)

                    succeeded = true
                } catch (e: Exception) {
                    LOGGER.error("Error produced when calling POST $baseUrl$endpoint. {}", e.message)
                    baseUrl = changeBaseUrl(baseUrl, backupUrls)
                }
            }

            checkNotNull(answer)
        }
    }

    private suspend inline fun <reified BodyType : Any, reified ResponseType> post(
        host: String,
        endpoint: String,
        body: BodyType
    ): ResponseType {
        checkSync(host)

        val uri = "$host$endpoint"
        LOGGER.debug("POST {} \n {}", uri, body)

        val response = nodeWebClient.post()
            .uri(uri)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(body), BodyType::class.java)
            .retrieve()
            .onStatus(HttpStatusCode::is5xxServerError) { error ->
                error.bodyToMono(String::class.java).map { OperationFailedException(it) }
            }
            .onStatus(HttpStatusCode::is4xxClientError) { error ->
                error.bodyToMono(String::class.java).map { OperationFailedException(it) }
            }
            .bodyToMono(ResponseType::class.java).awaitSingle()

        LOGGER.debug("Response:\n {}", response)
        return response
    }

    private suspend inline fun <reified ResponseType : Any> genericGet(
        endpoint: String,
        host: String? = null,
    ): ResponseType? {
        return if (host != null) {
            get(host, endpoint)
        } else {

            var baseUrl = mainUrl
            var answer: ResponseType? = null

            var succeeded = false

            while (!succeeded) {
                try {
                    answer = get(baseUrl, endpoint)

                    succeeded = true
                } catch (e: Exception) {
                    LOGGER.error("Error produced when calling GET $baseUrl$endpoint. {}", e.message)
                    baseUrl = changeBaseUrl(baseUrl, backupUrls)
                }
            }

            answer
        }
    }

    private suspend inline fun <reified ResponseType : Any> get(
        host: String,
        endpoint: String,
        checkSync: Boolean = true,
    ): ResponseType? {
        if(checkSync) checkSync(host)

        val uri = "$host$endpoint"
        LOGGER.debug("GET {}", uri)

        return try {
            nodeWebClient.get()
                .uri(uri)
                .awaitExchange {
                    when {
                        it.statusCode().is2xxSuccessful -> {
                            try {
                                it.awaitBody()
                            } catch (e: Exception) {
                                e.printStackTrace()
                                throw e
                            }
                        }

                        it.statusCode().is4xxClientError -> throw NodeException.NotFoundException()
                        else -> throw NodeException.NodeAccessException()
                    }
                }
        } catch (notFoundException: NodeException.NotFoundException) {
            return null
        }
    }

    private suspend fun checkSync(host: String) {
        LOGGER.debug("Check tezos node synchronization")
        val now = OffsetDateTime.now()

        val cached = syncCheckCache[host]
        val timestamp = if (cached == null || cached.lastCheckDate.isBefore(now.minus(syncCheckCacheDuration))) {
            val headBlockHeader = get<TzBlockHeaderResponse>(
                endpoint = "/chains/main/blocks/$HEAD_BLOCK/header",
                host = host,
                checkSync = false, // Avoid stack overflow
            )
            val newValue = CachedValue(
                // There is always a head therefore we can cast not null with !!
                blockTimestamp = checkNotNull(headBlockHeader).timestamp,
                lastCheckDate = now,
            )

            // Update cache
            syncCheckCache[host] = newValue

            newValue.blockTimestamp
        } else {
            cached.blockTimestamp
        }

        if (timestamp.isBefore(now.minus(syncThreshold))) {
            throw NodeException.NodeDeSyncException(host)
        }
    }

    private fun getBlockId(blockHash: String, predecessorOffset: Long): String {
        return if (predecessorOffset == 0L) {
            blockHash
        } else {
            "$blockHash~$predecessorOffset"
        }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(NodeConnector::class.java)
    }
}
