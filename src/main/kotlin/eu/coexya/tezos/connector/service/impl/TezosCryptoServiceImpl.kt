package eu.coexya.tezos.connector.service.impl

import eu.coexya.tezos.connector.enums.EncoderType
import eu.coexya.tezos.connector.model.Ed25519KeyPair
import eu.coexya.tezos.connector.service.TezosCryptoService
import eu.coexya.tezos.connector.service.TezosCryptoService.Companion.DOUBLE_CHECKSUM_SIZE
import eu.coexya.tezos.connector.service.TezosCryptoService.Companion.EDPK_PREFIX
import eu.coexya.tezos.connector.service.TezosCryptoService.Companion.EDSK_PREFIX
import eu.coexya.tezos.connector.utils.first
import eu.coexya.tezos.connector.utils.mid
import org.bitcoinj.core.Base58
import org.bouncycastle.crypto.AsymmetricCipherKeyPair
import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator
import org.bouncycastle.crypto.params.Ed25519KeyGenerationParameters
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters
import org.bouncycastle.crypto.signers.Ed25519Signer
import org.bouncycastle.util.encoders.Hex
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.security.SecureRandom
import java.text.MessageFormat

@Service
class TezosCryptoServiceImpl : TezosCryptoService {

    override fun generateEd25519KeyPair(): Ed25519KeyPair {

        //Log the call:
        LOGGER.debug("generateIdentity()")

        // Generate the key pair:
        val keyPairGenerator = Ed25519KeyPairGenerator()
        keyPairGenerator.init(Ed25519KeyGenerationParameters(SecureRandom()))
        val asymmetricCipherKeyPair: AsymmetricCipherKeyPair = keyPairGenerator.generateKeyPair()

        try {


            // Extract the private and public key:
            val publicKey = asymmetricCipherKeyPair.public as Ed25519PublicKeyParameters
            val privateKey = asymmetricCipherKeyPair.private as Ed25519PrivateKeyParameters


            // Instantiate the TezosIdentity with the right formatted keys and address:
            val result = Ed25519KeyPair(publicKey.encoded, privateKey.encoded)


            // Log and return the result:
            LOGGER.debug("result = {}", result)
            return result
        } catch (e: Exception) {
            throw Exception("An exception occurred while generating an ED25519 key pair: ${e.message}", e)
        }

    }

    override fun encode(payload: ByteArray, encoderType: EncoderType): String {
        LOGGER.debug("encode (payload = $payload, encoderType = $encoderType")
        return try {
            when (encoderType) {
                EncoderType.HEX -> Hex.toHexString(payload)
                EncoderType.BASE58 -> Base58.encode(payload)
            }
        } catch (e: Exception) {
            throw Exception("An exception occurred while encoding $payload to $encoderType: ${e.message}")
        }
    }

    override fun decode(payload: String, encoderType: EncoderType): ByteArray {
        LOGGER.debug("decode (payload = $payload, encoderType = $encoderType)")
        return try {
            when (encoderType) {
                EncoderType.HEX -> Hex.decode(payload)
                EncoderType.BASE58 -> Base58.decode(payload)
            }
        } catch (e: Exception) {
            val message = MessageFormat.format("An exception occurred while decoding $payload: ${e.message}")
            throw Exception(message)
        }
    }

    override fun createSignature(payload: ByteArray, privateKey: String): ByteArray {
        try {

            // Extract the private key from the complex private key:
            val rawPrivateKey: ByteArray = extractRawPrivateKey(privateKey)

            // Prepare the ED25591 parameters:
            val parameters = Ed25519PrivateKeyParameters(rawPrivateKey, 0)


            // Generate the signature:
            var signature: ByteArray
            val signer = Ed25519Signer()
            synchronized(signer) {
                signer.init(true, parameters)
                signer.reset()
                signer.update(payload, 0, payload.size)
                signature = signer.generateSignature()
            }

            //
            LOGGER.debug("signature.length = {}", signature.size)
            return signature
        } catch (e: Exception) {
            throw Exception("An exception occurred while creating a signature: ${e.message}", e)
        }
    }

    override fun extractRawPublicKey(tezosPublicKey: String): ByteArray {
        LOGGER.debug("extractRawPublicKey (tezosPublicKey = $tezosPublicKey")

        try {

            // Decode the public key:
            val decodedPublicKey = Base58.decode(tezosPublicKey.trim())

            // Extract the public key (32 bit after the prefix):
            val rawPublicKey = mid(decodedPublicKey, EDPK_PREFIX.size, 32)

            //Log and return the value:
            LOGGER.debug("rawPublicKey = 0x${Hex.toHexString(rawPublicKey)}")
            return rawPublicKey
        }catch (e: Exception){
            throw Exception("An exception occurred while extracting the raw public key: ${e.message}", e)
        }
    }

    override fun extractRawPrivateKey(tezosPrivateKey: String): ByteArray {

        // Log the call:
        LOGGER.debug("extractRawPrivateKey (tezosPrivateKey = $tezosPrivateKey)")

        try {

            // Decode the private key:
            val decodePrivateKey = Base58.decode(tezosPrivateKey.trim())

            // Extract the private key (32 bit after the prefix):
            val rawPrivateKey = mid(decodePrivateKey, EDSK_PREFIX.size, 32)

            LOGGER.debug("rawPrivateKey = {}", "0x" + Hex.toHexString(rawPrivateKey))
            return rawPrivateKey
        } catch (e: Exception) {
            throw Exception("An exception occurred while extracting the raw private key: ${e.message}", e)
        }
    }

    override fun computeDoubleCheckSum(payload: ByteArray): ByteArray {
        LOGGER.debug("computeDoubleCheckSum (payload = $payload")

        try {
            val checksum = first(
                sha256digester.digest(sha256digester.digest(payload)),
                DOUBLE_CHECKSUM_SIZE
            )

            LOGGER.debug("checksum = ${Hex.toHexString(checksum)}")
            return checksum
        } catch (e: Exception) {
            throw Exception("Cannot compute check sum: ${e.message}", e)
        }
    }

    companion object{
        private val LOGGER = LoggerFactory.getLogger(TezosCryptoServiceImpl::class.java)
    }
}