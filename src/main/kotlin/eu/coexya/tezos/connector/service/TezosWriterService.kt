package eu.coexya.tezos.connector.service

import eu.coexya.tezos.connector.model.TezosIdentity

interface TezosWriterService {

    /**
     * Anchor a root hash within the Tezos blockchain.
     * @param rootHash Root hash to anchor.
     * @param signerIdentity Crypto identity of the signer.
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    suspend fun anchorHash(rootHash: String, signerIdentity: TezosIdentity): String

    /**
     * Anchor multiple root hash within the Tezos blockchain.
     * @param rootHashList Root hash list to anchor.
     * @param signerIdentity Crypto identity of the signer.
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    suspend fun anchorHash(rootHashList: List<String>, signerIdentity: TezosIdentity): String

    /**
     * Anchor multiple root hash within the Tezos blockchain.
     * @param rootHashWithSmartContracts Root hash to anchor with contract address as key and root hash as List.
     * @param signerIdentity Crypto identity of the signer.
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    suspend fun anchorHashMultiSmartContract(
        rootHashWithSmartContracts: Map<String, List<String>>,
        signerIdentity: TezosIdentity
    ): String

    /**
     * Generate a new key pair, the generated key pair is associated
     * with an empty account that has not yet been revealed.
     * @return Crypto identity of the new account.
     */
    fun generateNewIdentity(): TezosIdentity

    /**
     * Generate a new key pair, the generated key pair is associated
     * with an empty account that has not yet been revealed.
     * @param from Crypto identity of debited account.
     * @param to Public address of the credited account.
     * @param amount The amount transferred.
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    suspend fun transferTezos(from: TezosIdentity, to: String, amount: Long): String

    /**
     * Reveal an account
     * @param identity Crypto identity to reveal
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    suspend fun revealIdentity(identity: TezosIdentity): String

}
