package eu.coexya.tezos.connector.service.impl

import eu.coexya.tezos.connector.contract.HashTimestamping
import eu.coexya.tezos.connector.contract.RevealService
import eu.coexya.tezos.connector.contract.TransferService
import eu.coexya.tezos.connector.model.TezosIdentity
import eu.coexya.tezos.connector.service.TezosCryptoService
import eu.coexya.tezos.connector.service.TezosUtilService
import eu.coexya.tezos.connector.service.TezosWriterService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class TezosWriterServiceImpl(
    private val tezosUtilService: TezosUtilService,
    private val hashTimestamping: HashTimestamping,
    private val transferService: TransferService,
    private val revealService: RevealService,
    private val tezosCryptoService: TezosCryptoService,
) : TezosWriterService {

    override suspend fun anchorHash(rootHash: String, signerIdentity: TezosIdentity): String {
        LOGGER.debug("anchorHash (rootHash = {}, signer = {})", rootHash, signerIdentity)
        return anchorHash(listOf(rootHash), signerIdentity)
    }

    override suspend fun anchorHash(rootHashList: List<String>, signerIdentity: TezosIdentity): String {
        LOGGER.debug("anchorHash (rootHashList = {}, signer = {})", rootHashList, signerIdentity)

        return hashTimestamping.timestampHash(rootHashList, signerIdentity)
    }

    override suspend fun anchorHashMultiSmartContract(
        rootHashWithSmartContracts: Map<String, List<String>>,
        signerIdentity: TezosIdentity
    ): String {
        LOGGER.debug(
            "anchorHash (rootHashWithSmartContract = {}, signer = {})",
            rootHashWithSmartContracts,
            signerIdentity
        )
        return hashTimestamping.timestampHashMultiSmartContract(rootHashWithSmartContracts, signerIdentity)
    }

    override fun generateNewIdentity(): TezosIdentity {
        LOGGER.debug("generateNewIdentity")

        val keyPair =
            tezosCryptoService.generateEd25519KeyPair()

        val newIdentity = tezosUtilService.retrieveIdentity(keyPair)

        LOGGER.debug("generated new identity: {}", newIdentity)
        return newIdentity
    }

    override suspend fun transferTezos(from: TezosIdentity, to: String, amount: Long): String {
        LOGGER.debug("transferTezos (from = {}, to = {}, amount = {})", from.publicAddress, to, amount)

        val transactionHash = transferService.transfer(from, to, amount)

        LOGGER.debug("transferred tezos (transactionId = {})", transactionHash)
        return transactionHash
    }

    override suspend fun revealIdentity(identity: TezosIdentity): String {
        LOGGER.debug("revealIdentity (identity = {})", identity)

        val transactionHash = revealService.reveal(identity)

        LOGGER.debug("revealed an account (transactionId = {})", transactionHash)
        return transactionHash
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(TezosWriterServiceImpl::class.java)
    }
}
