package eu.coexya.tezos.connector.service.impl

import eu.coexya.tezos.connector.enums.EncoderType
import eu.coexya.tezos.connector.model.Ed25519KeyPair
import eu.coexya.tezos.connector.model.TezosIdentity
import eu.coexya.tezos.connector.service.TezosCryptoService
import eu.coexya.tezos.connector.service.TezosUtilService
import eu.coexya.tezos.connector.utils.hexStringToByteArray
import eu.coexya.tezos.connector.utils.join
import eu.coexya.tezos.connector.utils.mid
import org.bouncycastle.crypto.Signer
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters
import org.bouncycastle.crypto.signers.Ed25519Signer
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.nio.charset.StandardCharsets

@Service
class TezosUtilServiceImpl(
    private val tezosCryptoService: TezosCryptoService,
) : TezosUtilService {

    override fun retrieveIdentity(publicKeyBase58: String, secretKeyBase58: String): TezosIdentity {
        LOGGER.debug("retrieveIdentity (publicKey = {}, secretKey = {})", publicKeyBase58, secretKeyBase58)

        // Retrieve the key pair:
        // Note that extraRawPublicKey is used to extract secret key as well due to key length consideration.
        val publicKeyBytes: ByteArray =
            tezosCryptoService.extractRawPublicKey(publicKeyBase58)
        val secretKeyBytes: ByteArray =
            tezosCryptoService.extractRawPublicKey(secretKeyBase58)
        val keyPair = Ed25519KeyPair(publicKeyBytes, secretKeyBytes)

        // Instantiate the TezosIdentity with the right formatted keys and address:
        val result = retrieveIdentity(keyPair)

        // Log and return the result:
        LOGGER.debug("identify retrieved: {}", result)
        return result
    }

    override fun unpackSignerAddress(packedAddress: String): String {
        LOGGER.debug("Unpacking signer address '$packedAddress'")
        // Remove the encoded prefix
        val addressWithoutPrefix = mid(hexStringToByteArray(packedAddress), 2, 20)

        // Prepend the prefix
        val prependedData = join(TezosCryptoService.TZ1_PREFIX, addressWithoutPrefix)

        // Add the checksum
        val checksum = tezosCryptoService.computeDoubleCheckSum(prependedData)
        val prependedDataWithChecksum = join(prependedData, checksum)

        return tezosCryptoService.encode(prependedDataWithChecksum, EncoderType.BASE58)
    }

    override fun computeScriptExpr(packedData: ByteArray): String {
        val digestedData = tezosCryptoService.blake2b256digester.digest(packedData)

        val prependedData = join(hexStringToByteArray("0d2c401b"), digestedData)

        val checksum = tezosCryptoService.computeDoubleCheckSum(prependedData)
        val prependedDataWithChecksum = join(prependedData, checksum)
        return tezosCryptoService.encode(prependedDataWithChecksum, EncoderType.BASE58)
    }

    override fun checkKeyPairValidity(publicKeyBase58: String, secretKeyBase58: String): Boolean {
        val publicKeyBytes: ByteArray =
            tezosCryptoService.extractRawPublicKey(publicKeyBase58)
        val secretKeyBytes: ByteArray =
            tezosCryptoService.extractRawPublicKey(secretKeyBase58)

        val publicKey = Ed25519PublicKeyParameters(publicKeyBytes, 0)
        val privateKey = Ed25519PrivateKeyParameters(secretKeyBytes, 0)

        // the message
        val message = "Message to sign".toByteArray(StandardCharsets.UTF_8)

        // create the signature
        val signer: Signer = Ed25519Signer()
        signer.init(true, privateKey)
        signer.update(message, 0, message.size)
        val signature = signer.generateSignature()

        // verify the signature
        val verifier: Signer = Ed25519Signer()
        verifier.init(false, publicKey)
        verifier.update(message, 0, message.size)

        return verifier.verifySignature(signature)
    }

    override fun retrieveIdentity(keyPair: Ed25519KeyPair): TezosIdentity {
        // Format the public key:
        val tezosPublicKeyRaw =
            join(TezosCryptoService.EDPK_PREFIX, keyPair.publicKey)
        val tezosPublicKeyRawChecksum =
            tezosCryptoService.computeDoubleCheckSum(tezosPublicKeyRaw)
        val tezosCheckedPublicKeyRaw =
            join(tezosPublicKeyRaw, tezosPublicKeyRawChecksum)
        val tezosPublicKey =
            tezosCryptoService.encode(tezosCheckedPublicKeyRaw, EncoderType.BASE58)

        // Format the private key:
        val tezosPrivateKeyRaw =
            join(TezosCryptoService.EDSK_PREFIX, keyPair.privateKey, keyPair.publicKey)
        val tezosPrivateKeyRawChecksum =
            tezosCryptoService.computeDoubleCheckSum(tezosPrivateKeyRaw)
        val tezosCheckedPrivateKeyRaw =
            join(tezosPrivateKeyRaw, tezosPrivateKeyRawChecksum)
        val tezosPrivateKey =
            tezosCryptoService.encode(tezosCheckedPrivateKeyRaw, EncoderType.BASE58)

        // Format the public address:
        val digestedPublicKey =
            tezosCryptoService.blake2b160digester.digest(keyPair.publicKey)
        val tezosPublicAddressRaw = join(TezosCryptoService.TZ1_PREFIX, digestedPublicKey)
        val tezosPublicAddressRawChecksum =
            tezosCryptoService.computeDoubleCheckSum(tezosPublicAddressRaw)
        val tezosPublicAddress = tezosCryptoService.encode(
            join(tezosPublicAddressRaw, tezosPublicAddressRawChecksum),
            EncoderType.BASE58
        )

        // Instantiate the TezosIdentity with the right formatted keys and address:
        return TezosIdentity(
            publicAddress = tezosPublicAddress,
            publicKey = tezosPublicKey,
            privateKey = tezosPrivateKey,
        )
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(TezosUtilServiceImpl::class.java)
    }

}
