package eu.coexya.tezos.connector.service.impl

import eu.coexya.tezos.connector.model.ForgeOp
import eu.coexya.tezos.connector.model.PreApplyOperation
import eu.coexya.tezos.connector.model.RunAndPreApplyOperationResult
import eu.coexya.tezos.connector.model.RunOperation
import eu.coexya.tezos.connector.node.NodeConnector
import eu.coexya.tezos.connector.node.exception.AddressNotFoundException
import eu.coexya.tezos.connector.node.exception.NodeException
import eu.coexya.tezos.connector.node.model.*
import eu.coexya.tezos.connector.service.TezosReaderService
import eu.coexya.tezos.connector.service.TezosUtilService
import eu.coexya.tezos.connector.utils.hexStringToByteArray
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class TezosReaderServiceImpl(
    private val nodeConnector: NodeConnector,
    val tezosUtilService: TezosUtilService,
) : TezosReaderService {

    override suspend fun getProtocol(host: String?): String {
        return nodeConnector.getProtocols(host)?.protocol ?: throw NodeException.NodeAccessException()
    }

    override suspend fun hashAlreadyExists(contractAddress: String, hash: String): Boolean {
        checkContractExists(contractAddress)
        val response = getValueFromContractStorage(contractAddress, hash)
        return response != null
    }

    override suspend fun getValueFromContractStorage(contractAddress: String, rootHash: String): StorageResponse? {
        if (contractAddress.isBlank() || rootHash.isBlank()) throw IllegalArgumentException("Arguments should not be blank.")
        val storageId = nodeConnector.getStorageId(contractAddress) ?: return null
        val scriptExpr = tezosUtilService.computeScriptExpr(packData(rootHash))
        return nodeConnector.getValueFromStorage(storageId, scriptExpr)
    }

    override suspend fun getContractStorageId(contractAddress: String): String? {
        return nodeConnector.getStorageId(contractAddress)
    }

    @Deprecated("Duplicate of getValueFromContractStorage")
    override suspend fun getValueFromContractBigMap(contractAddress: String, rootHash: String): StorageResponse? {
        if (contractAddress.isBlank() || rootHash.isBlank()) throw IllegalArgumentException("Arguments should not be blank.")

        val bigMapId = nodeConnector.getStorageId(contractAddress) ?: return null
        val scriptExpr = tezosUtilService.computeScriptExpr(packData(rootHash))
        return nodeConnector.getValueFromStorage(bigMapId, scriptExpr)
    }

    override suspend fun checkContractExists(contractAddress: String, host: String?) {
        if (nodeConnector.getBalanceOrNull(contractAddress, host) == null) {
            throw AddressNotFoundException(contractAddress)
        }
    }

    @Deprecated("Duplicate of getContractStorageId")
    override suspend fun getContractBigMapId(contractAddress: String): String? {
        return nodeConnector.getStorageId(contractAddress)
    }

    override suspend fun getTezosAccountBalance(publicAddress: String): Long {
        return nodeConnector.getBalance(publicAddress)
    }

    override suspend fun getTezosAccountCounter(publicAddress: String, host: String?): Long {
        return nodeConnector.getCounter(publicAddress, host)
    }

    override suspend fun getTransaction(
        contractAddress: String,
        blockHash: String,
        transactionHash: String
    ): TzOperationResponse? {
        LOGGER.debug("Getting transaction '$transactionHash' on block '$blockHash', with contract '$contractAddress'")
        return nodeConnector.getBlockOperations(contractAddress, blockHash)
            .find {
                it.hash == transactionHash
            }
    }

    override suspend fun getBlockDepth(blockHash: String): Long? {
        LOGGER.debug("Getting block '$blockHash' depth")
        val headLevel = getHeadBlockLevel()
        val blockLevel = nodeConnector.getBlockLevel(blockHash) ?: return null
        return headLevel - blockLevel
    }

    override suspend fun getBlockHash(blockHash: String, predecessorOffset: Long): String? {
        return nodeConnector.getBlockHash(blockHash, predecessorOffset)
    }

    override suspend fun getChainId(host: String?): String {
        return nodeConnector.getChainId()
    }

    override suspend fun packData(hash: String): ByteArray {
        return hexStringToByteArray(nodeConnector.packData(hash).packedData)
    }

    override suspend fun hasBakingSlotIn(delegateAddress: String, range: Long): Boolean {
        val headLevel = getHeadBlockLevel()
        for (i in 0..range) {
            if (nodeConnector.isBakingSlot(delegateAddress, headLevel + i)) {
                return true
            }
        }
        return false
    }

    override suspend fun hasBakingSlotAt(delegateAddress: String, level: Long): Boolean {
        return nodeConnector.isBakingSlot(delegateAddress, level)
    }

    override suspend fun getBakingSlots(delegateAddress: String, priority: Int, cycle: Int): List<BakingRight>? {
        return nodeConnector.getBakingSlots(delegateAddress, priority, cycle)
    }

    override suspend fun dryRun(operation: TzOperation): PreApplyResponse {
        return nodeConnector.dryRun(operation)
    }

    override suspend fun runOperation(bodyToPost: RunOperation, host: String?): RunAndPreApplyOperationResult {
        return nodeConnector.runOperation(bodyToPost, host)
    }

    override suspend fun forgeOperation(forgeOperation: ForgeOp, host: String?): String {
        return nodeConnector.forgeOperation(forgeOperation, host)
    }

    override suspend fun preApplyOperation(preApplyOperation: PreApplyOperation, host: String?): String {
        return nodeConnector.preApplyOperation(preApplyOperation, host)
    }

    override suspend fun injectOperation(injectOperation: String, host: String?): String {
        return nodeConnector.injectOperation(injectOperation, host)
    }

    override suspend fun getBlock(blockHash: String, predecessorOffset: Long, host: String?): TzBlock? {
        return nodeConnector.getBlock(blockHash, predecessorOffset, host)
    }

    override suspend fun getBlockString(blockHash: String): String? {
        return nodeConnector.getBlockString(blockHash)
    }

    override suspend fun getBlockLevel(blockHash: String, predecessorOffset: Long): Long? {
        return nodeConnector.getBlockLevel(blockHash, predecessorOffset)
    }

    override suspend fun getBlockOperationHashes(blockHash: String, predecessorOffset: Long): List<String>? {
        return nodeConnector.getBlockOperationHashes(blockHash, predecessorOffset)
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(TezosReaderServiceImpl::class.java)
    }

}
