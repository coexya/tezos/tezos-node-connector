package eu.coexya.tezos.connector.service

import eu.coexya.tezos.connector.model.ForgeOp
import eu.coexya.tezos.connector.model.PreApplyOperation
import eu.coexya.tezos.connector.model.RunAndPreApplyOperationResult
import eu.coexya.tezos.connector.model.RunOperation
import eu.coexya.tezos.connector.node.exception.AddressNotFoundException
import eu.coexya.tezos.connector.node.model.*
import eu.coexya.tezos.connector.utils.HEAD_BLOCK

interface TezosReaderService {

    /**
     * Get the current protocol of the Tezos Blockchain
     * @param host optional param to force the request to specific host tezos node
     * @return the protocol as String
     */
    suspend fun getProtocol(host: String? = null): String

    /**
     * Check if the provided hash is already in the contract storage.
     * @param contractAddress Address of the smart contract which storage may contain the provided hash.
     * @param hash Hash to check existence.
     * @return Existence of the provided hash in the given contract storage.
     */
    suspend fun hashAlreadyExists(contractAddress: String, hash: String): Boolean

    /**
     * Get the storage value of a given rootHash
     * @param contractAddress Address of the smart contract which storage may contain the provided hash.
     * @param rootHash Hash to get information.
     * @return Existence of the provided hash in the given contract storage.
     */
    suspend fun getValueFromContractStorage(contractAddress: String, rootHash: String): StorageResponse?

    /**
     * Get the storage id of a smart contract
     * @param contractAddress Address of the smart contract
     * @return The storage id of the smart ocntract as String
     */
    suspend fun getContractStorageId(contractAddress: String): String?

    /**
     * Get the bigMap value of a given rootHash
     * @param contractAddress Address of the smart contract which storage may contain the provided hash.
     * @param rootHash Hash to get information.
     * @return Existence of the provided hash in the given contract storage.
     */
    @Deprecated("Duplicate of getValueFromContractStorage")
    suspend fun getValueFromContractBigMap(contractAddress: String, rootHash: String): StorageResponse?

    /**
     * Check if the smart contract with address [contractAddress] exists on the blockchain
     * @param host optional param to force the request to specific host tezos node
     * throws [AddressNotFoundException] if it does not
     */
    suspend fun checkContractExists(contractAddress: String, host: String? = null)

    /**
     * Get the bigMap id of a smart contract
     * @param contractAddress Address of the smart contract
     */
    @Deprecated("Duplicate of getContractStorageId")
    suspend fun getContractBigMapId(contractAddress: String): String?

    /**
     * Retrieve the balance of a given account
     * @param publicAddress hash of the account
     * @return the balance of the given account, will be 0 if the account doesn't exist
     */
    suspend fun getTezosAccountBalance(publicAddress: String): Long

    /**
     * Retrieve the counter of a given account
     * @param publicAddress hash of the account
     * @param host optional param to force the request to specific host tezos node
     * @return the counter of the given account, will be -1 if the account doesn't exist
     */
    suspend fun getTezosAccountCounter(publicAddress: String, host: String? = null): Long

    /**
     * Find an operation on the blockchain with the blockHash of the block on which it was anchored
     * @param contractAddress Address of the smart contract related to the operation.
     * @param blockHash Hash of the block on which the operation was anchored
     * @param transactionHash Hash of the transaction to find
     */
    suspend fun getTransaction(
        contractAddress: String,
        blockHash: String,
        transactionHash: String
    ): TzOperationResponse?

    /**
     * Get the depth of a given block, null if the block don't exist
     * @param blockHash Hash of the block on which the operation was anchored
     */
    suspend fun getBlockDepth(blockHash: String): Long?

    /**
     * Get the hash of the block
     */
    suspend fun getBlockHash(blockHash: String, predecessorOffset: Long = 0L): String?

    /**
     * Get the hash of the head block
     */
    suspend fun getHeadBlockHash(offset: Long = 0) = getBlockHash(HEAD_BLOCK, offset)!!

    /**
     * Get the chainId
     * @param host optional param to force the request to specific host tezos node
     */
    suspend fun getChainId(host: String? = null): String

    /**
     * Calls API to pack data
     * @param hash Hash to pack
     */
    suspend fun packData(hash: String): ByteArray

    /**
     * Checks if the given baker has baking rights in the next range levels
     * @param delegateAddress the baker
     * @param range the range to check
     * @return True if there is a baking slot, False else
     */
    suspend fun hasBakingSlotIn(delegateAddress: String, range: Long): Boolean

    /**
     * Checks if the given baker has baking rights in the given level
     * @param delegateAddress the baker
     * @param level the level to check
     * @return True if there is a baking slot, False else
     */
    suspend fun hasBakingSlotAt(delegateAddress: String, level: Long): Boolean

    /**
     * Retrieves baking slots for a given delegate and levels
     * @param delegateAddress filter on the baker
     * @param priority filter on the priority (usually 0)
     * @param cycle filter on the cycle
     * @return All slots of the given delegate
     */
    suspend fun getBakingSlots(delegateAddress: String, priority: Int, cycle: Int): List<BakingRight>?

    /**
     * Dry runs an operation
     * @param operation the operation to dry run
     * @return PreApplyResponse if dry run is successful
     */
    suspend fun dryRun(operation: TzOperation): PreApplyResponse

    /**
     * run an operation
     * @param bodyToPost the operation to run
     * @param host optional param to force the request to specific host tezos node
     * @return RunOperationResult as String if run is successful
     */
    suspend fun runOperation(bodyToPost: RunOperation, host: String? = null): RunAndPreApplyOperationResult

    /**
     * forge an operation
     * @param forgeOperation the operation to forge
     * @param host optional param to force the request to specific host tezos node
     * @return String if forge is successful
     */
    suspend fun forgeOperation(forgeOperation: ForgeOp, host: String? = null): String

    /**
     * Pre apply an operation
     * @param preApplyOperation the operation to preApply
     * @param host optional param to force the request to specific host tezos node
     * @return PreOperationResult as String if preApply is successful
     */
    suspend fun preApplyOperation(preApplyOperation: PreApplyOperation, host: String? = null): String

    /**
     * Inject an operation
     * @param injectOperation the operation to inject
     * @param host optional param to force the request to specific host tezos node
     * @return the transaction of the hash as String if inject is successful
     */
    suspend fun injectOperation(injectOperation: String, host: String? = null): String

    /**
     * Get a block from the blockchain
     * @param blockHash the hash of the block to get
     * @param host optional param to force the request to specific host tezos node
     * @return The block
     */
    suspend fun getBlock(blockHash: String, predecessorOffset: Long = 0, host: String? = null): TzBlock?

    /**
     * Get a block from the blockchain
     * @param blockHash the hash of the block to get
     * @return The block as a string
     */
    suspend fun getBlockString(blockHash: String): String?

    /**
     * Get head block
     * @param host optional param to force the request to specific host tezos node
     * @return The block
     */
    suspend fun getHeadBlock(offset: Long = 0, host: String? = null) = getBlock(HEAD_BLOCK, offset, host)!!

    /**
     * Get block level from the blockchain
     * @param blockHash the hash of the block to get
     * @return The level
     */
    suspend fun getBlockLevel(blockHash: String, predecessorOffset: Long = 0): Long?

    /**
     * Get head block level from the blockchain
     * @return The level
     */
    suspend fun getHeadBlockLevel(offset: Long = 0) = getBlockLevel(HEAD_BLOCK, offset)!!

    /**
     * Gets hashes of the operations on a given block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     * @param predecessorOffset Offset to get a predecessor of the given blockHash
     */
    suspend fun getBlockOperationHashes(blockHash: String, predecessorOffset: Long = 0L): List<String>?

    /**
     * Gets hashes of the operations on head block
     */
    suspend fun getHeadBlockOperationHashes(offset: Long = 0) = getBlockOperationHashes(HEAD_BLOCK, offset)!!

}
