package eu.coexya.tezos.connector.service

import eu.coexya.tezos.connector.enums.EncoderType
import eu.coexya.tezos.connector.enums.Type
import eu.coexya.tezos.connector.model.*
import eu.coexya.tezos.connector.node.exception.AddressNotFoundException
import eu.coexya.tezos.connector.node.exception.NodeException
import eu.coexya.tezos.connector.utils.changeBaseUrl
import eu.coexya.tezos.connector.utils.join
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.math.ceil

interface TezosAbstractService {

    val tezosReaderService: TezosReaderService
    val tezosContractAddress: String?
    val tezosCryptoService: TezosCryptoService

    val mainUrl: String
    val backupUrls: List<String>?

    suspend fun invoke(parameters: OperationParameters, tezosIdentity: TezosIdentity): String {
        LOGGER.info("Start invoke")
        LOGGER.debug("invoke (parameters = {})", parameters)
        return processOperation(parameters, tezosIdentity)
    }

    private suspend fun processOperation(
        parameters: OperationParameters,
        tezosIdentity: TezosIdentity
    ): String {
        LOGGER.info("Start processOperation")
        LOGGER.debug("processOperation (parameters = {})", parameters)

        var baseUrl = mainUrl
        var answer = ""

        var succeeded = false

        while (!succeeded) {
            try {

                val tezosCounter = tezosReaderService.getTezosAccountCounter(
                    publicAddress = tezosIdentity.publicAddress,
                    host = baseUrl
                ) + 1

                if (tezosCounter == 0L && (parameters.type != Type.REVEAL)) {
                    throw AddressNotFoundException("The address ${tezosIdentity.publicAddress} has not been found.")
                }

                val runOperationResult = runOperation(parameters, tezosCounter, tezosIdentity, baseUrl)

                val forgeOperationResult = forgeOperation(runOperationResult, tezosCounter, baseUrl)

                val signOperationResult = signOperation(forgeOperationResult, tezosIdentity.privateKey)

                preApplyOperation(signOperationResult, runOperationResult, forgeOperationResult, tezosCounter, baseUrl)

                answer = injectOperation(forgeOperationResult, signOperationResult, baseUrl)

                succeeded = true
            } catch (e: Exception) {
                if (e is NodeException.NodeDeSyncException) {
                    // No need to produce too much logs for de-sync exception
                    LOGGER.error("Error during operation process. {}", e.message)
                } else {
                    LOGGER.error("Error during operation process.", e)
                }
                baseUrl = changeBaseUrl(baseUrl, backupUrls)
            }
        }

        return answer
    }

    /**
     * Pretend to anchor the operation in order to get the storage and gas limit of each transaction in the operation
     */
    private suspend fun runOperation(
        parameters: OperationParameters,
        counter: Long,
        tezosIdentity: TezosIdentity,
        host: String,
    ): RunAndPreApplyOperationResult {
        LOGGER.info("Start runOperation")
        LOGGER.debug("runOperation (parameters = {}, counter = {})", parameters, counter)

        // Check existence of smart contracts
        val smartContractAddresses = parameters.hashesBySC.keys
        smartContractAddresses.forEach { tezosReaderService.checkContractExists(it, host) }

        val blockHash = tezosReaderService.getHeadBlock(host = host).hash
        val runOperationBody = createRunOperationBody(parameters, counter, blockHash, tezosIdentity, host)
        val operationResult = tezosReaderService.runOperation(runOperationBody, host)
        operationResult.blockHash = blockHash
        return operationResult
    }

    /**
     * Forge the operation to convert it into a sequence of bytes that will represent the operation
     */
    private suspend fun forgeOperation(
        runOperationResult: RunAndPreApplyOperationResult,
        counter: Long,
        host: String,
    ): ForgeOperationResult {
        LOGGER.info("Start forgeOperation")
        LOGGER.debug("forgeOperation (runOperationResult = {}, counter = {})", runOperationResult, counter)
        val bodyToPost = createForgeOperationBody(runOperationResult, counter)

        // Forge the operation a first time with the fee default value to 0 for all the transactions within it
        val forgeToGetFees = tezosReaderService.forgeOperation(bodyToPost, host)
        val feesList: MutableList<Long> = mutableListOf()

        // Calculate the fee for each transaction and replace their default value by their calculated fee
        for (content in bodyToPost.contents) {
            // fees >= (minimal_fees + minimal_nanotez_per_byte * size + minimal_nanotez_per_gas_unit * gas)
            // minimal_fees = 0.000 1ꜩ (100µꜩ)
            // minimal_nanotez_per_byte = 1µꜩ/B
            // minimal_nanotez_per_gas_unit = 0,1µꜩ/gu
            //
            // fees >= (minimal_fees + size + gas/10)
            // size = number of bytes = forgeToGetFees/2

            val fee = 100 + forgeToGetFees.length / (bodyToPost.contents.size) + content.gas_limit.toLong() / 10
            feesList.add(fee)
            content.fee = fee.toString()
        }

        // Forge the real operation with the correct fees and add 0x03 that will indicate that:
        // it's a hexadecimal number (0x)
        // it represents a forge operation (03)
        val forgeOperationResult: String = "0x03" + tezosReaderService.forgeOperation(bodyToPost, host)

        return ForgeOperationResult(
            result = forgeOperationResult,
            feesList = feesList,
        )
    }

    /**
     * Create a signature based on the sequence of bytes returned by the forgeOperation and encode it
     */
    private fun signOperation(
        forgeOperationResult: ForgeOperationResult,
        adminPrivateKey: String,
    ): SignOperationResult {
        LOGGER.info("Start signOperation")
        LOGGER.debug("signOperation (forgeOperation = {}, adminPrivateKey = *** )", forgeOperationResult)

        // Extract the forged data:
        val rawForgedData = tezosCryptoService.decode(
            forgeOperationResult.result.substring(2),
            EncoderType.HEX
        )

        // Compute the forged data hash:
        val rawForgedDataHash = tezosCryptoService.blake2b256digester.digest(rawForgedData)

        // Generate the signature:
        val signature = synchronized(tezosCryptoService) {
            tezosCryptoService.createSignature(rawForgedDataHash, adminPrivateKey)
        }

        //
        val rawSignatureAsHex = "0x" + tezosCryptoService.encode(signature, EncoderType.HEX)

        // We group the signature with its prefix and compute the double checksum:
        val signatureWithPrefix = join(TezosCryptoService.EDSIG_PREFIX, signature)
        val signatureWithPrefixChecksum = tezosCryptoService.computeDoubleCheckSum(signatureWithPrefix)

        // We join the prefixed signature and the checksum:
        val signatureWithPrefixAndChecksum = join(signatureWithPrefix, signatureWithPrefixChecksum)

        // We encode the all stuff with B58:
        val encodedSignature =
            tezosCryptoService.encode(signatureWithPrefixAndChecksum, EncoderType.BASE58)

        return SignOperationResult(
            encodedSignature = encodedSignature,
            rawSignatureAsHex = rawSignatureAsHex,
        )
    }

    /**
     * Check if all previous steps were done correctly
     * If there is an error, the post operation will fail and send an error
     */
    private suspend fun preApplyOperation(
        signOperationResult: SignOperationResult,
        runOperationResult: RunAndPreApplyOperationResult,
        forgeOperationResult: ForgeOperationResult,
        counter: Long,
        host: String,
    ) {
        LOGGER.info("Start preApplyOperation")
        LOGGER.debug(
            "preApplyOperation (signOperationResult = {}, runOperationResult = {}, forgeOperationResult = {})",
            signOperationResult,
            runOperationResult,
            forgeOperationResult,
        )

        val bodyToPost =
            createPreApplyOperationBody(signOperationResult, runOperationResult, forgeOperationResult, counter, host)

        // Will throw an error if the previous steps were done incorrectly
        tezosReaderService.preApplyOperation(bodyToPost, host)
    }

    /**
     * Inject the transaction in the blockchain
     */
    private suspend fun injectOperation(
        forgeOperationResult: ForgeOperationResult,
        signOperationResult: SignOperationResult,
        host: String,
    ): String {
        LOGGER.info("Start InjectOperation")
        LOGGER.debug(
            "InjectOperation (forgeOperationResult = {}, signOperationResult = {}",
            forgeOperationResult,
            signOperationResult
        )

        val bodyToPost = createInjectOperationBody(forgeOperationResult, signOperationResult)

        return tezosReaderService.injectOperation(bodyToPost, host)
    }

    /**
     * Create an object that will be turned into a json to be post during the runOperation
     */
    private suspend fun createRunOperationBody(
        parameters: OperationParameters,
        counter: Long,
        blockHash: String,
        tezosIdentity: TezosIdentity,
        host: String,
    ): RunOperation {
        val chainId = tezosReaderService.getChainId(host)
        var count = counter
        val contents = mutableListOf<Content>()

        when (parameters.type) {
            Type.TRANSACTION -> {
                val rootHashesMultiSmartContract = parameters.hashesBySC
                for (item in rootHashesMultiSmartContract) {
                    for (hash in item.value) {
                        contents.add(
                            Content(
                                kind = "transaction",
                                source = tezosIdentity.publicAddress,
                                counter = count.toString(),
                                amount = "0",
                                destination = item.key,
                                parameters = Parameters(value = Value(string = hash)),
                            )
                        )
                        count++
                    }
                }
            }

            Type.TRANSFER -> {
                assert(parameters.accounts.size == parameters.amounts.size)
                for (i in parameters.accounts.indices) {
                    contents.add(
                        Content(
                            kind = "transaction",
                            source = tezosIdentity.publicAddress,
                            counter = count.toString(),
                            amount = parameters.amounts[i],
                            destination = parameters.accounts[i]
                        )
                    )
                }
            }

            Type.REVEAL -> {
                for (publicKey in parameters.publicKeys) {
                    contents.add(
                        Content(
                            kind = "reveal",
                            source = tezosIdentity.publicAddress,
                            counter = count.toString(),
                            public_key = publicKey,
                        )
                    )
                }
            }
        }

        return RunOperation(
            operation = Operation(
                contents = contents,
                branch = blockHash,
                signature = DUMMY_SIGNATURE,
            ),
            chain_id = chainId,
        )
    }

    /**
     * Create an object that will be turned into a json to be post during the forgeOperation
     */
    private fun createForgeOperationBody(runOperationResult: RunAndPreApplyOperationResult, counter: Long): ForgeOp {
        var count = counter
        val contents = mutableListOf<Content>()

        for (content in runOperationResult.contents) {
            if (content.metadata!!.operation_result.status.trim().equals("applied", ignoreCase = true)) {
                contents.add(
                    Content(
                        kind = content.kind,
                        source = content.source,
                        fee = content.fee,
                        counter = count.toString(),
                        gas_limit = ((content.metadata.operation_result.consumed_milligas?.let { ceil(it.toInt() / 1000.0).toInt() }
                            ?: "0".toInt()) + GAS_MARGIN).toString(),
                        storage_limit = content.metadata.operation_result.paid_storage_size_diff ?: "0",
                        amount = content.amount,
                        destination = content.destination,
                        parameters = content.parameters,
                        public_key = content.public_key,
                    )
                )
                count++
            }
        }
        return ForgeOp(
            contents = contents,
            branch = runOperationResult.blockHash!!,
        )
    }

    /**
     * Create an object that will be turned into a json to be post during the PreApplyOperation
     */
    private suspend fun createPreApplyOperationBody(
        signOperationResult: SignOperationResult,
        runOperationResult: RunAndPreApplyOperationResult,
        forgeOperationResult: ForgeOperationResult,
        counter: Long,
        host: String,
    ): PreApplyOperation {
        val protocol = tezosReaderService.getProtocol(host)
        var count = counter
        val contents = mutableListOf<Content>()

        for (i in 0 until runOperationResult.contents.size) {
            val content = runOperationResult.contents[i]
            if (content.metadata!!.operation_result.status.trim().equals("applied", ignoreCase = true)) {
                contents.add(
                    Content(
                        kind = content.kind,
                        source = content.source,
                        fee = forgeOperationResult.feesList[i].toString(),
                        counter = count.toString(),
                        gas_limit = ((content.metadata.operation_result.consumed_milligas?.let { ceil(it.toInt() / 1000.0).toInt() }
                            ?: "0".toInt()) + GAS_MARGIN).toString(),
                        storage_limit = content.metadata.operation_result.paid_storage_size_diff ?: "0",
                        amount = content.amount,
                        destination = content.destination,
                        public_key = content.public_key,
                        parameters = content.parameters,
                    )
                )
                count++
            }
        }

        return PreApplyOperation(
            protocol = protocol,
            branch = runOperationResult.blockHash!!,
            contents = contents,
            signature = signOperationResult.encodedSignature
        )
    }

    /**
     * Create an object that will be turned into a json to be post during the injectOperation
     */
    private fun createInjectOperationBody(
        forgeOperationResult: ForgeOperationResult,
        signOperationResult: SignOperationResult
    ): String {
        return ("\"" + forgeOperationResult.result.substring(4) +
            signOperationResult.rawSignatureAsHex.substring(2) + "\"")
    }

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(TezosAbstractService::class.java)

        const val GAS_MARGIN = 100

        const val DUMMY_SIGNATURE =
            "edsigtkKBqNqYu4cuzJGKknmQ94t61Mp3QvZmhvU5e9wtWi8yWX9ZRJgTEcnbaUfi6BBnZuVuM4haVtKaHNTJLmduJA9CFjpq4e"
    }
}
