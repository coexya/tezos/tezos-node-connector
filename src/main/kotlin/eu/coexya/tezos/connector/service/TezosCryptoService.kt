package eu.coexya.tezos.connector.service

import eu.coexya.tezos.connector.enums.EncoderType
import eu.coexya.tezos.connector.model.Ed25519KeyPair
import org.bouncycastle.jcajce.provider.digest.Blake2b
import java.security.MessageDigest

interface TezosCryptoService {

    val blake2b256digester: MessageDigest
        get() = Blake2b.Blake2b256()

    val blake2b160digester: MessageDigest
        get() = Blake2b.Blake2b160()

    val sha256digester: MessageDigest
        get() = MessageDigest.getInstance("SHA-256")


    fun generateEd25519KeyPair(): Ed25519KeyPair

    fun encode(payload: ByteArray, encoderType: EncoderType): String

    fun decode(payload: String, encoderType: EncoderType): ByteArray

    fun createSignature(payload: ByteArray, privateKey: String): ByteArray

    fun extractRawPublicKey(tezosPublicKey: String): ByteArray

    fun extractRawPrivateKey(tezosPrivateKey: String): ByteArray

    fun computeDoubleCheckSum(payload: ByteArray): ByteArray

    companion object {

        const val DOUBLE_CHECKSUM_SIZE = 4

        val EDPK_PREFIX = byteArrayOf(13.toByte(), 15.toByte(), 37.toByte(), 217.toByte())
        val EDSK_PREFIX = byteArrayOf(43.toByte(), 246.toByte(), 78.toByte(), 7.toByte())
        val TZ1_PREFIX = byteArrayOf(6.toByte(), 161.toByte(), 159.toByte())
        val EDSIG_PREFIX = byteArrayOf(9.toByte(), 245.toByte(), 205.toByte(), 134.toByte(), 18.toByte())
    }
}