package eu.coexya.tezos.connector.configuration

import eu.coexya.tezos.connector.node.exception.NodeException
import io.netty.handler.logging.LogLevel
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.reactive.function.client.ExchangeStrategies
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient
import reactor.netty.resources.ConnectionProvider
import reactor.netty.transport.logging.AdvancedByteBufFormat
import java.security.SecureRandom
import java.time.Duration

@Configuration
@EnableAutoConfiguration
class TezosConfiguration {

    @Bean
    fun nodeWebClient(
        webClientBuilder: WebClient.Builder
    ): WebClient {
        val webClient = try {
            webClientBuilder
                .clientConnector(
                    ReactorClientHttpConnector(
                        HttpClient.create(
                            ConnectionProvider
                                .builder("custom-provider")
                                .maxConnections(500)
                                .pendingAcquireTimeout(Duration.ofSeconds(45))
                                .maxIdleTime(Duration.ofSeconds(600))
                                .build()
                        ).wiretap(
                            "reactor.netty.http.client.HttpClient",
                            LogLevel.DEBUG,
                            AdvancedByteBufFormat.TEXTUAL
                        )
                    )
                )
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .exchangeStrategies(
                    ExchangeStrategies.builder()
                        .codecs {
                            it
                                .defaultCodecs()
                                .maxInMemorySize(16 * 1024 * 1024)
                        }
                        .build()
                )
                .build()
        } catch (e: Exception) {
            throw NodeException.NodeAccessException()
        }
        return webClient
    }

    @Bean
    fun secureRandom() = SecureRandom()
}
