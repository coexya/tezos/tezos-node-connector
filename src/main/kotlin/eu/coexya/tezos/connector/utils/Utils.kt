package eu.coexya.tezos.connector.utils

import eu.coexya.tezos.connector.node.exception.NodeException

fun changeBaseUrl(baseUrl: String, backupUrls: List<String>?): String {
    val urls = backupUrls ?: throw NodeException.NodeAccessException()

    val index = urls.indexOf(baseUrl)
    return if (index < 0) {
        urls[0]
    } else if (index < urls.size - 1) {
        urls[index + 1]
    } else {
        throw NodeException.NodeAccessException()
    }
}

const val HEAD_BLOCK = "head"
