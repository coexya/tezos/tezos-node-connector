package eu.coexya.tezos.connector.utils

import java.lang.reflect.InvocationTargetException
import java.text.MessageFormat


fun join(vararg a: ByteArray): ByteArray {
    var size = 0
    for (arr in a) {
        size += arr.size
    }
    val result = ByteArray(size)
    var index = 0
    for (arr in a) {
        val arrLen = arr.size
        System.arraycopy(arr, 0, result, index, arrLen)
        index += arrLen
    }
    return result
}

fun mid(source: ByteArray, index: Int, length: Int): ByteArray {
    checkState(
        source.size >= index + length, "source is too small",
        Exception::class.java
    )
    val result = ByteArray(length)
    for (i in 0 until length) {
        result[i] = source[i + index]
    }
    return result
}

fun first(source: ByteArray, length: Int): ByteArray {
    checkState(
        source.size >= length, "source is too small",
        Exception::class.java
    )
    val result = ByteArray(length)
    for (i in 0 until length) {
        result[i] = source[i]
    }
    return result
}

private fun <T : Throwable?> checkState(condition: Boolean, message: String?, clazz: Class<T>) {
    try {
        var exceptionMessage = message
        if (!condition) {
            if (exceptionMessage == null) {
                exceptionMessage = "condition is not fulfilled"
            }
            val exception = clazz.getConstructor(String::class.java).newInstance(exceptionMessage)
            throw Exception(exception)
        }
    } catch (ex: NoSuchMethodException) {
        val exceptionMessage = MessageFormat
            .format(
                "An exception occurred while executing checkState: {0}",
                ex.message
            )
        throw RuntimeException(exceptionMessage, ex)
    } catch (ex: InstantiationException) {
        val exceptionMessage = MessageFormat
            .format(
                "An exception occurred while executing checkState: {0}",
                ex.message
            )
        throw RuntimeException(exceptionMessage, ex)
    } catch (ex: IllegalAccessException) {
        val exceptionMessage = MessageFormat
            .format(
                "An exception occurred while executing checkState: {0}",
                ex.message
            )
        throw RuntimeException(exceptionMessage, ex)
    } catch (ex: InvocationTargetException) {
        val exceptionMessage = MessageFormat
            .format(
                "An exception occurred while executing checkState: {0}",
                ex.message
            )
        throw RuntimeException(exceptionMessage, ex)
    }
}