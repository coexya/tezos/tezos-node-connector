package eu.coexya.tezos.connector.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import eu.coexya.tezos.connector.configuration.TestConfiguration
import eu.coexya.tezos.connector.node.NodeConnector
import eu.coexya.tezos.connector.node.model.BigMapId
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.io.File

@SpringBootTest(classes = [TestConfiguration::class])
@ActiveProfiles("test")
@Disabled("Cannot run this in tests, depends on the blockchain.")
class ChangeUrlTest(
    @Autowired private val nodeConnector: NodeConnector
) {

    @Test
    fun testChangeUrl() {
        runBlocking {
            nodeConnector.getBalance("tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6")
        }
    }

    @Test
    fun testGetBigMapId() {
        runBlocking {
            val json = File("./src/test/resources/jsonFiles/testGetBigMapId1.json")
                .readText(Charsets.UTF_8)
            val mapped = jacksonObjectMapper().readValue(json, BigMapId::class.java)
            assert(mapped?.int == "4")
            val json2 = File("./src/test/resources/jsonFiles/testGetBigMapId2.json")
                .readText(Charsets.UTF_8)
            val mapped2 = jacksonObjectMapper().readValue(json2, BigMapId::class.java)
            assert(mapped2?.int == "52")
            val int3 = nodeConnector.getStorageId("KT1WfWoUwz66hmgzneFREmgJkHHKWq8qwST2")
            assert(int3 == "5")
        }
    }
}
