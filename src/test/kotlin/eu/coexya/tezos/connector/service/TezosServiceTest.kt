package eu.coexya.tezos.connector.service

import com.ninjasquad.springmockk.MockkBean
import eu.coexya.tezos.connector.configuration.TestConfiguration
import eu.coexya.tezos.connector.node.NodeConnector
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles


@SpringBootTest(classes = [TestConfiguration::class])
@ActiveProfiles("test")
class TezosServiceTest(
    @Autowired private val tezosUtilService: TezosUtilService,
    @Autowired private val tezosReaderService: TezosReaderService
) {

    @MockkBean
    private lateinit var nodeConnector: NodeConnector

    @ParameterizedTest
    @MethodSource("balanceTestProvider")
    fun `balance is correct`(
        expectedBalance: Long,
        publicAddress: String
    ) {
        // Useless test, TODO
        coEvery {
            nodeConnector.getBalance(any())
        } returns expectedBalance

        runBlocking {
            assertEquals(expectedBalance, tezosReaderService.getTezosAccountBalance(publicAddress))
        }
    }

    @ParameterizedTest
    @MethodSource("validityTestProvider")
    fun `validity test is correct`(
        publicKey: String,
        privateKey: String,
        expected: Boolean
    ) {
        assertEquals(
            expected,
            tezosUtilService.checkKeyPairValidity(publicKeyBase58 = publicKey, secretKeyBase58 = privateKey)
        )
    }

    @Test
    fun `validity throws when key is not correctly formatted`() {
        assertThrows<Exception> {
            tezosUtilService.checkKeyPairValidity(
                publicKeyBase58 = "not a public key",
                secretKeyBase58 = bobPrivateKey
            )
        }
        assertThrows<Exception> {
            tezosUtilService.checkKeyPairValidity(
                publicKeyBase58 = bobPublicKey,
                secretKeyBase58 = "not a private key"
            )
        }
    }

    @ParameterizedTest
    @MethodSource("tezosAccounts")
    fun retrieveIdentityTest(
        name: String,
        publicKey: String,
        secretKey: String,
        publicAddress: String,
        privateKey: String
    ) {
        val tezosIdentity = tezosUtilService.retrieveIdentity(publicKey, secretKey)
        assertEquals(publicAddress, tezosIdentity.publicAddress)
        assertEquals(privateKey, tezosIdentity.privateKey)
    }


    companion object {

        const val bobPublicAddress = "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6"
        const val bobPublicKey = "edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4"
        const val bobPrivateKey = "edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt"

        const val alicePublicAddress = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"
        const val alicePublicKey = "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn"
        const val alicePrivateKey = "edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq"

        @JvmStatic
        fun tezosAccounts() = listOf(
            Arguments.of(
                "alice",
                alicePublicKey,
                alicePrivateKey,
                alicePublicAddress,
                "edskRpm2mUhvoUjHjXgMoDRxMKhtKfww1ixmWiHCWhHuMEEbGzdnz8Ks4vgarKDtxok7HmrEo1JzkXkdkvyw7Rtw6BNtSd7MJ7"
            ),
            Arguments.of(
                "bob",
                bobPublicKey,
                bobPrivateKey,
                bobPublicAddress,
                "edskRpthz6CnXsmvknNHAozBJGax6e5Hj9q2bK3j8u4XJRgvAgkGHTvy9Q8ksNsbtMmU2JzsC2wXD3BhMQx346QxWzjDVsTZzG"
            )
        )

        @JvmStatic
        fun validityTestProvider(): List<Arguments> {
            return listOf(
                Arguments.of(
                    bobPublicKey,
                    bobPrivateKey,
                    true
                ),
                Arguments.of(
                    alicePublicKey,
                    alicePrivateKey,
                    true
                ),
                Arguments.of(
                    bobPublicKey,
                    alicePrivateKey,
                    false
                ),
                Arguments.of(
                    alicePrivateKey,
                    bobPublicKey,
                    false
                )
            )
        }

        @JvmStatic
        fun balanceTestProvider(): List<Arguments> {
            return listOf(
                Arguments.of(
                    0L,
                    "wrong type of string"
                )
            )
        }

    }

}
