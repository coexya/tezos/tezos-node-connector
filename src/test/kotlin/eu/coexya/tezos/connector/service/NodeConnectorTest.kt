package eu.coexya.tezos.connector.service

import eu.coexya.tezos.connector.contract.HashTimestamping
import eu.coexya.tezos.connector.contract.RevealService
import eu.coexya.tezos.connector.contract.TransferService
import eu.coexya.tezos.connector.model.TezosIdentity
import eu.coexya.tezos.connector.node.NodeConnector
import eu.coexya.tezos.connector.node.model.TzBlock
import eu.coexya.tezos.connector.service.impl.TezosReaderServiceImpl
import eu.coexya.tezos.connector.service.impl.TezosWriterServiceImpl
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.web.reactive.function.client.WebClient
import java.time.Duration
import java.time.OffsetDateTime

@Disabled("Cannot run this in tests, depends on the blockchain.")
@SpringBootTest(classes = [eu.coexya.tezos.connector.configuration.TestConfiguration::class])
@ActiveProfiles("test")
class NodeConnectorTest @Autowired constructor(
    private val tezosUtilService: TezosUtilService,
    hashTimestamping: HashTimestamping,
    transferService: TransferService,
    revealService: RevealService,
    tezosCryptoService: TezosCryptoService,
) {

    private val webClientNode = WebClient.builder()
        .baseUrl("https://testnet.tezos.coexya.eu/")
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .build()

    private val nodeConnector = NodeConnector(
        nodeWebClient = webClientNode,
        backupUrls = null,
        mainUrl = "",
        syncThreshold = Duration.ofMinutes(5),
        syncCheckCacheDuration = Duration.ofMinutes(5),
    )

    private val tezosReaderService =
        TezosReaderServiceImpl(
            nodeConnector = nodeConnector,
            tezosUtilService = tezosUtilService
        )

    private val tezosWriterServiceTest =
        TezosWriterServiceImpl(
            tezosUtilService = tezosUtilService,
            hashTimestamping = hashTimestamping,
            transferService = transferService,
            revealService = revealService,
            tezosCryptoService = tezosCryptoService,
        )

    private val tezosWriterServiceTestGigaNode =
        TezosWriterServiceImpl(
            tezosUtilService = tezosUtilService,
            hashTimestamping = hashTimestamping,
            transferService = transferService,
            revealService = revealService,
            tezosCryptoService = tezosCryptoService,
        )

    private val tezosWriterServiceProdSmartPy =
        TezosWriterServiceImpl(
            tezosUtilService = tezosUtilService,
            hashTimestamping = hashTimestamping,
            transferService = transferService,
            revealService = revealService,
            tezosCryptoService = tezosCryptoService,
        )

    private val tezosWriterServiceProdGigaNode =
        TezosWriterServiceImpl(
            tezosUtilService = tezosUtilService,
            hashTimestamping = hashTimestamping,
            transferService = transferService,
            revealService = revealService,
            tezosCryptoService = tezosCryptoService,
        )

    private val tezosWriterServiceProdECAD =
        TezosWriterServiceImpl(
            tezosUtilService = tezosUtilService,
            hashTimestamping = hashTimestamping,
            transferService = transferService,
            revealService = revealService,
            tezosCryptoService = tezosCryptoService,
        )

    private val tezosWriterServiceProdBlockscale =
        TezosWriterServiceImpl(
            tezosUtilService = tezosUtilService,
            hashTimestamping = hashTimestamping,
            transferService = transferService,
            revealService = revealService,
            tezosCryptoService = tezosCryptoService,
        )

    private val tezosWriterServiceProdLetzBake =
        TezosWriterServiceImpl(
            tezosUtilService = tezosUtilService,
            hashTimestamping = hashTimestamping,
            transferService = transferService,
            revealService = revealService,
            tezosCryptoService = tezosCryptoService,
        )

    private val tezosWriterServiceProdSword =
        TezosWriterServiceImpl(
            tezosUtilService = tezosUtilService,
            hashTimestamping = hashTimestamping,
            transferService = transferService,
            revealService = revealService,
            tezosCryptoService = tezosCryptoService,
        )

    @Test
    fun testAnchorTest() {
        runBlocking {
            val tezosIdentity: TezosIdentity = tezosUtilService.retrieveIdentity(
                "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn",
                "edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq"
            )

            tezosWriterServiceTestGigaNode.anchorHash(
                listOf("6961f6169be222df64d1907f6e5d4fb465a5def93ebd8c1d903e174bee980218"),
                tezosIdentity
            )
        }
    }


    @Test
    fun testAnchorProd() {
        runBlocking {
            val tezosIdentity: TezosIdentity = tezosUtilService.retrieveIdentity(
                "xxx",
                "xxx"
            )
            tezosWriterServiceProdSword.anchorHash(
                listOf("6961f6169be222df64d1907f6e5d4fb465a5def93ebd8c1d903e174bee980221"),
                tezosIdentity
            )
        }
    }


    @Test
    fun testGetBlockOperations() {
        val result =
            runBlocking {
                nodeConnector.getBlockOperations(
                    CONTRACT_ADDRESS,
                    BLOCK_HASH
                )
            }

        Assertions.assertEquals(1, result.size)

        val signer = result[0].packedSignerAddress
        Assertions.assertEquals(SIGNER_ADDRESS_1, tezosUtilService.unpackSignerAddress(signer))

        val noOperation =
            runBlocking {
                nodeConnector.getBlockOperations(
                    CONTRACT_ADDRESS,
                    INEXISTANT_BLOCK_HASH
                )
            }
        Assertions.assertNotNull(noOperation)
        Assertions.assertTrue(noOperation.isEmpty())
    }

    @ParameterizedTest
    @MethodSource("hashAlreadyExistsProvider")
    fun testHashAlreadyExists(
        expected: Boolean,
        contractAddress: String,
        documentHash: String,
    ) {
        runBlocking {
            Assertions.assertEquals(expected, tezosReaderService.hashAlreadyExists(contractAddress, documentHash))
        }
    }


    @Test
    fun testGetValueFromStorage() {
        val result =
            runBlocking {
                tezosReaderService.getValueFromContractStorage(
                    CONTRACT_ADDRESS,
                    EXISTING_ROOT_HASH_1
                )
            }
        Assertions.assertNotNull(result)
        Assertions.assertEquals(SIGNER_ADDRESS_1, result!!.signerAddress)
        Assertions.assertEquals(SIGNATURE_TIME, result.timestamp)

        val notSigned =
            runBlocking {
                tezosReaderService.getValueFromContractStorage(
                    CONTRACT_ADDRESS,
                    NON_EXISTING_ROOT_HASH
                )
            }
        Assertions.assertNull(notSigned)
    }

    @Test
    fun testHashAlreadyExistsNoBlankArgs(
    ) {
        assertThrows<IllegalArgumentException> {
            runBlocking {
                tezosReaderService.hashAlreadyExists("", "")
            }
        }
        assertThrows<IllegalArgumentException> {
            runBlocking {
                tezosReaderService.hashAlreadyExists("something", "")
            }
        }
        assertThrows<IllegalArgumentException> {
            runBlocking {
                tezosReaderService.hashAlreadyExists("", "something")
            }
        }
    }

    @Test
    fun testGetBalance() {
        val op1 =
            runBlocking {
                nodeConnector.getBalance(SIGNER_ADDRESS_1)
            }
        Assertions.assertTrue(op1 > 0L)

        val op2 =
            runBlocking {
                nodeConnector.getBalance("tz1SwomhzFtkQwt7pDY9V6fXh6VDa8eC6ET2")
            }
        Assertions.assertEquals(0, op2)
    }

    @Test
    fun testGetBlockOperationHashes() {
        val op1 =
            runBlocking {
                nodeConnector.getBlockOperationHashes(BLOCK_HASH)
            }

        Assertions.assertNotNull(op1)
        Assertions.assertTrue(op1!!.contains(OPERATION_HASH))

        runBlocking {
            tezosReaderService.getHeadBlockOperationHashes()
        }
    }

    @ParameterizedTest
    @MethodSource("getBlockLevelProvider")
    fun testGetBlockLevel(
        blockHash: String,
        expectedLevel: Long?
    ) {
        val blockLevel =
            runBlocking {
                nodeConnector.getBlockLevel(blockHash)
            }
        Assertions.assertEquals(expectedLevel, blockLevel)
    }

    @Test
    fun testGetBlockHash() {
        val op2 =
            runBlocking {
                nodeConnector.getBlockHash(BLOCK_HASH)
            }
        Assertions.assertEquals(BLOCK_HASH, op2)

    }

    @Test
    fun testGetHeadBlockHash() {
        val op2 =
            runBlocking {
                tezosReaderService.getHeadBlockHash()
            }
        Assertions.assertEquals('B', op2.first())
    }

    @Test
    fun testGetBlock() {
        val op2 =
            runBlocking {
                nodeConnector.getBlock(BLOCK_HASH)
            }
        Assertions.assertEquals(
            TzBlock(
                hash = BLOCK_HASH,
                header = TzBlock.Header(
                    level = 1465202,
                    timestamp = SIGNATURE_TIME
                ),
                metadata = mapOf("baker" to ""),
                operations = emptyList(),
            ), op2
        )
    }

    @Suppress("unused")
    companion object {
        const val CONTRACT_ADDRESS = "KT1Acfs1M5FXHGYQpvdKUwGbZtrUkqrisweJ"
        const val SIGNER_ADDRESS_1 = "tz1Sdxu3CT3anp5qbBo2ooUdYherqGwgXDuJ"
        const val OPERATION_HASH = "oo8HjL6xkHmhW5J2Jzdp4NbUa3P2ZpU3q94ppzdVLS21N3pPPWZ"
        const val BLOCK_HASH = "BLFGeatxUsuZaAjz7FKYbKVfNNc7HJFXZuugfJinHZVQSMbhGsE"
        const val INEXISTANT_BLOCK_HASH = "BMFm7vSMYMvjoyDibHRfo7tkW71LNAFpNTMUo1iGZJvn5Kx1ntu"

        const val EXISTING_ROOT_HASH_1 = "c034805fac82a6c72c5ad08e422ecd4dbb15182ff19bbbfa3a1147686a108181"
        const val NON_EXISTING_ROOT_HASH = "f5b13a1dfd05f9e6392db0e6ae5423e78fa8ff9a4ee817cc5ed9b0dcdfe04f3f"

        val SIGNATURE_TIME: OffsetDateTime = OffsetDateTime.parse("2021-05-10T06:00:06Z")

        @JvmStatic
        fun hashAlreadyExistsProvider() = listOf(
            Arguments.of(
                true,
                CONTRACT_ADDRESS,
                EXISTING_ROOT_HASH_1
            ),
            Arguments.of(
                false,
                CONTRACT_ADDRESS,
                NON_EXISTING_ROOT_HASH
            )
        )

        @JvmStatic
        fun getBlockLevelProvider() = listOf(
            Arguments.of(
                INEXISTANT_BLOCK_HASH,
                null
            ),
            Arguments.of(
                BLOCK_HASH,
                1465202L
            )
        )
    }

}
